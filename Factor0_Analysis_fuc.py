"""
与fuc0相比
1. 规范了注释
2. 增加了换手率，分层收益表格包含所有层
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
from matplotlib import cm
import scipy.stats
import concurrent
import numpy as np
#import Factor0_Markowitz_portfolio as Markowitz
import concurrent.futures

# 图画面板调整为白色
rc = {'axes.facecolor': 'white',
      'savefig.facecolor': 'white'}
mpl.rcParams.update(rc)

# 显示负号
mpl.rcParams['axes.unicode_minus'] = False

# 显示中文黑体
mpl.rcParams['font.sans-serif'] = ['SimHei']

# 提高照片清晰度 dpi:dots per inch
mpl.rcParams['figure.dpi'] = 200

# 设置样式
# plt.style.use('_mpl-gallery')


class FactorAnalysis:
    def __init__(self, factor, ret, list_periods, n_group,asset_weight):
        self.factor = factor
        self.ret = ret
        self.list_periods = list_periods
        self.n_group = n_group
        self.rf = 0.02
        self.asset_weight = asset_weight

    def summary_group_ic(self, group_list):
        # 在每一个调仓期间
        summary = []
        for n_group in group_list:
            results = []
            adj_date = []
            for adj_idx in range(0, len(self.factor), 1):
                adj_day = self.factor.index[adj_idx]
                adj_date.append(adj_day)
                print(adj_day)  # 这是根据因子值调仓日，在开盘的时候，用的open数据

                # 获取每个周期的因子值和收益率，并合并
                df_1 = self.factor.iloc[adj_idx, :].to_frame().copy()
                df_1.columns = ['factor']
                df_1 = pd.concat([df_1, self.ret.iloc[adj_idx: adj_idx + 1, :].copy().T],
                                 axis=1)

                # 根据factor的值去掉nan，没有用到未来的股价信息
                df_1.dropna(subset=['factor'], inplace=True)

                # 给factor添加一个很小的随机扰动
                df_1['factor'] += np.random.normal(0, 1, size=(df_1.shape[0])) * 1e-18

                # 先不考虑分组不够的情况

                # ==============================
                # 计算该调仓周期的各股收益率
                # ==============================
                # 如果调仓第一天有nan，说明买不进去，可以砍掉这一条股票，因为交易第二天确实买不进去
                df_1.dropna(subset=[df_1.columns[1]], inplace=True)
                # 如果调仓第一天之后有nan，保持收益率为0不变，cumsum会砍掉部分收益
                df_1.iloc[:, 2:] = df_1.iloc[:, 2:].fillna(0)
                df_1['ret'] = np.cumprod(1 + df_1.iloc[:, 1:], axis=1).iloc[:, -1] - 1

                df_2 = df_1.loc[:, ['factor', 'ret']].copy()
                # 计算每个组的分割线，并fillna对数据进行分组
                # 有个小缺点就是最后一组的数据可能会少一些
                df_2.sort_values(by=['factor'], ascending=True, inplace=True)
                df_quantile = df_2['factor'][np.arange(0, len(df_2), int(np.ceil(len(df_2) / n_group)))].to_frame()
                df_quantile['group'] = np.arange(1, n_group + 1)
                # 根据factor进行分组
                df_2['group'] = pd.qcut(df_2['factor'], n_group, labels=list(range(1, n_group + 1)))

                # 计算分组收益率
                # groupby会直接过滤掉ret为nan的股票，也就说你买不进去，符合常理
                df_ret = df_2.groupby(by=['group'])['ret'].mean().to_frame()  # 用普通收益率计算的分组收益率
                corr = scipy.stats.spearmanr(df_ret.index, df_ret['ret'])[0]
                results.append(corr)

            summary.append(results)
        df_summary = pd.DataFrame(data=summary, columns=adj_date,
                                  index=group_list)
        return df_summary

    # ==============================
    # ==============================
    # 计算return，ic，rank_ic，换手率，每组数量
    # ==============================
    # ==============================

    def calc_results(self, adj_periods):  # 限定一个调仓周期
        adj_date = []
        ret_results = pd.DataFrame()
        excess_ret_results = pd.DataFrame()
        ex_results = pd.DataFrame()
        ic_list = []
        rank_ic_list = []
        df_last = pd.DataFrame()
        df_group_num = pd.DataFrame()
        group_ic_list = []
        df_group_inner_ic = pd.DataFrame()

        reg_beta_list = []
        reg_intercept_list = []
        reg_r_value_list = []
        reg_p_value_list = []
        reg_std_err_list = []
        reg_t_value_list = []

        asset_weight = self.asset_weight
        df_2_combined = pd.DataFrame()  #储存过去每期每组的组号和ret，没有进行组内合并前

        df_2_factor = pd.DataFrame()
        df_2_ret = pd.DataFrame()
        df_2_group =  pd.DataFrame()


        # 在每一个调仓期间
        for adj_idx in range(0, len(self.factor), adj_periods):  # 如0，5，10，15....调仓时间点，限定在一个周期内部
            adj_day = self.factor.index[adj_idx]
            adj_date.append(adj_day)  # 调仓日期
            # print(adj_day)  # 这是根据因子值调仓日，在开盘的时候，用的open数据

            # 获取每个周期的因子值和收益率，并合并
            df_1 = self.factor.iloc[adj_idx, :].to_frame()  # 调仓当天(周期第一天）所有标的的因子值，作为1列
            df_1.columns = ['factor']
            df_1 = pd.concat([df_1, self.ret.iloc[adj_idx: adj_idx + adj_periods, :].T],
                             axis=1)  # 周期内部，索引是标的物代码，第1列为因子值，其他列为周期内的每日收益率

            # 根据factor的值去掉nan，没有用到未来的股价信息
            df_1.dropna(subset=['factor'], inplace=True)
            # 给factor添加一个很小的随机扰动
            df_1['factor'] += np.random.normal(0, 1, size=(df_1.shape[0])) * 1e-12

            # 先不考虑分组不够的情况
            """
            计算该调仓周期的各股收益率
            如果调仓第一天有nan，说明买不进去，砍掉。这里没有用到未来数据，因为确实买不进去
            """
            df_1.dropna(subset=[df_1.columns[1]], inplace=True)
            df_1.iloc[:, 2:] = df_1.iloc[:, 2:].fillna(0)              # 如果调仓第一天之后有nan，保持收益率为0不变，cumsum会砍掉部分收益
            df_1['ret'] = np.cumprod(1 + df_1.iloc[:, 1:], axis=1).iloc[:, -1] - 1   # 计算调仓周期内的收益率，增加一列，横着遍历每一天计算周期结束时的累计收益率

            # 根据group进行分组收益率的计算，增加一列，改列标记每个标的物的组号，就是层号
            df_2 = df_1.loc[:, ['factor', 'ret']].copy()
            df_2['group'] = pd.qcut(df_2['factor'], self.n_group, labels=list(range(1, self.n_group + 1)))

            # # 计算每组内部的ic情况，把不同组的ic存入一个list,疑问，为什么是和索引检验性关系，而不是和因子
            # inner_ic_list = []
            # for group_idx in range(1, self.n_group+1):
            #    inner_ic = df_2[df_2['group'] == group_idx]
            #    print('inner_ic.index是这个\n',inner_ic.index)
            #    inner_ic = scipy.stats.spearmanr(inner_ic.index, inner_ic['ret'])[0]
            #    inner_ic_list.append(inner_ic)
            # df_inner_ic = pd.DataFrame(data=inner_ic_list,
            #                           index=list(range(1, self.n_group+1)),
            #                           columns=[adj_day])
            # df_group_inner_ic = pd.concat([df_group_inner_ic, df_inner_ic], axis=1)

            """
            记录一下每一组包含的股票个数，方便查看是否异常
            """
            df_group_num_tmp = df_2['group'].value_counts().to_frame()
            df_group_num_tmp.columns = [adj_day]
            df_group_num = pd.concat([df_group_num, df_group_num_tmp], axis=1)

            """
            计算每个调仓周期的累计收益率、因子值、分组组号
            计算分组收益率，也就是每一期，不同组的累计收益率的组内均值，如果每期有10组，则有10个均值
            groupby会直接过滤掉ret为nan的股票，也就说你买不进去，符合常理
            """
            if df_2_ret.empty:
                df_2_ret = df_2['ret'].to_frame()
                df_2_ret.columns = [adj_day]
                df_2_factor = df_2['factor'].to_frame()
                df_2_factor.columns = [adj_day]
                df_2_group = df_2['group'].to_frame()
                df_2_group.columns = [adj_day]
                df_2_combined = pd.concat([ df_2['group'].to_frame(), df_2_ret ], axis=1)
            else:
                df_for = df_2['ret'].to_frame()
                df_for.columns = [adj_day]
                df_2_ret = pd.concat([df_2_ret, df_for ], axis=1)
                df_fac = df_2['factor'].to_frame()
                df_fac.columns = [adj_day]
                df_2_factor = pd.concat([df_2_factor, df_fac], axis=1)
                g_for = df_2['group'].to_frame()
                g_for.columns = [adj_day]
                df_2_group = pd.concat([df_2_group, g_for], axis=1)
                df_2_combined = pd.concat([ df_2['group'].to_frame() , df_2_ret, df_2['ret'].to_frame() ], axis=1)   #每次调仓group都要更新,依次是group,之前的ret,今天的ret

            """
            计算分组收益率
            """
            if asset_weight == 'mean':
                df_ret = df_2.groupby(by=['group'])['ret'].mean().to_frame()  # 用普通收益率计算的分组收益率，组内均值
            elif asset_weight == 'score':
                def mean_weighted(data):
                    return (data['ret'] * data['factor']).sum() / data['factor'].sum()
                df_ret = df_2.groupby(by=['group']).apply(mean_weighted)
            """ #用不到
            elif asset_weight == 'MVO':
                def mvo_weight(data):
                    data = pd.DataFrame(data)
                    #print('转置后的ret',data.iloc[:,1:].T)
                    #print('df_2',df_2)
                    if len(data.iloc[:,1:].T)  <= 100:  #样本太少可能不行
                        return data.iloc[:,-1].mean()
                    else:
                        result_dict= Markowitz.optimize_portfolio( data.iloc[:,1:].T ,risk_free_rate = 0, sellshort=False )
                        weights_df = pd.DataFrame(result_dict['Weights'], columns=['weight'])
                        weights_df.index = data.iloc[:, -1].index
                        #print(data.iloc[0,0],'的组内权重：',weights_df['weight'])
                        #print('合并后ret最后一列', data.iloc[:, -1])
                        return  ( data.iloc[:,-1] * weights_df['weight'] ).sum()

                print('df_2_combined', df_2_combined,'df_2_combined.groupby',df_2_combined.groupby(by=['group']))
                df_ret = df_2_combined.groupby(by=['group']).apply(mvo_weight)
            """
            df_ret.columns = [adj_day]
            ret_results = pd.concat([ret_results, df_ret], axis=1)

            df_ret_mean = df_2.groupby(by=['group'])['ret'].mean().to_frame()

            """
            计算分组的超额收益率
            """
            market_ret = df_2['ret'].mean()  # 周期内所有标的物  的累计收益率  的均值，作为市场收益率，每周期就1个值
            df_excess_ret = df_ret - market_ret
            excess_ret_results = pd.concat([excess_ret_results, df_excess_ret], axis=1)  # 每个周期一个超额数据框，

            """
            计算一下分层的ic,
            也就是计算   序号与 不同组的累计收益率的组内均值，的相关性，rank_ic
            """
            group_ic = scipy.stats.spearmanr(df_ret.index, df_ret)[0]  #原来是df_ret['ret']
            group_ic_list.append(group_ic)

            """
            计算每个周期所有标的物的因子值  与  周期内累计收益率，计算相关性，IC, IC_IR
            """
            ic = df_1.loc[:, ['factor', 'ret']].corr(method='pearson').iloc[0, 1]
            rank_ic = df_1.loc[:, ['factor', 'ret']].corr(method='spearman').iloc[0, 1]
            ic_list.append(ic)
            rank_ic_list.append(rank_ic)

            """
            单因子回归法测试
            """
            from scipy.stats import linregress, pearsonr, spearmanr
            X = df_1.loc[:, 'factor'].tolist()
            y = df_1.loc[:, 'ret'  ].tolist()
            slope, intercept, r_value, p_value, std_err = linregress(X, y)
            t_value = slope / std_err  # 计算 t 值
            reg_beta_list.append(slope)
            reg_intercept_list.append(intercept)
            reg_r_value_list.append(r_value)
            reg_p_value_list.append(p_value)
            reg_std_err_list.append(std_err)
            reg_t_value_list.append(t_value)

            """
            计算一下换手率，即上一期和这一期的股票重合度
            """
            if len(df_last) == 0: # 第一期没有换手率
                df_exchange = pd.DataFrame(data=[0] * self.n_group,
                                           index=list(np.arange(1, self.n_group + 1)),
                                           columns=[adj_day])
            else:
                ex_list = []
                for i in np.arange(1, self.n_group + 1):
                    stocks_now = df_2[df_2['group'] == i].index
                    stocks_last = df_last[df_last['group'] == i].index
                    # print(stock_last)
                    retain_rate = len(set(stocks_last).intersection(set(stocks_now))) / len(stocks_last)
                    exchange_rate = 1 - retain_rate
                    ex_list.append(exchange_rate)
                df_exchange = pd.DataFrame(data=ex_list,
                                           index=list(np.arange(1, self.n_group + 1)),
                                           columns=[adj_day])
            ex_results = pd.concat([ex_results, df_exchange], axis=1)
            df_last = df_2.copy()

        df_ic = pd.DataFrame(data=[ic_list, rank_ic_list],
                             index=['ic', 'rank_ic'],
                             columns=adj_date).T

        df_reg =  pd.DataFrame(data=[reg_beta_list,reg_intercept_list,reg_r_value_list,
                                            reg_p_value_list, reg_std_err_list, reg_t_value_list],
                             index=['beta', 'intercept','r_value','p_value','std_err','t_value'],
                             columns=adj_date).T

        df_group_ic = pd.DataFrame(data=group_ic_list,
                                   columns=['group_ic'],
                                   index=adj_date)

        results = {
            'adj_period': adj_periods,
            'ic': df_ic,
            'reg': df_reg,
            'ret_period_df':df_2_ret,
            'factor_period_df':df_2_factor,
            'group_period_df': df_2_group,
            'group_ic': df_group_ic,
            # 'group_inner_ic': df_group_inner_ic,
            'ret': ret_results,
            'excess_ret': excess_ret_results,
            'group_num': df_group_num,
            'exchange': ex_results
        }
        # print(len(results))

        return results


    def calc_multiple_periods(self):
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [executor.submit(self.calc_results, i) for i in self.list_periods]
        results = [f.result() for f in concurrent.futures.as_completed(futures)]
        results.sort(key=lambda e: e.get('adj_period'))  # 按照adj_period排序
        self.results = results  # 传回给self.results
        return results

    '''
    def calc_multiple_periods(self):
        # results = []
        # for i in self.list_periods:
        #     results.append(self.calc_results(i))

        with concurrent.futures.ProcessPoolExecutor() as executor:
            results = [executor.submit(self.calc_results, i)
                       for i in self.list_periods]
        results = [f.result() for f in concurrent.futures.as_completed(results)]
        results = sorted(results, key = lambda e: e.__getitem__('adj_period'))  # 按照adj_period排序
        self.results = results
        return results
    '''


    """
    IC的分析
    """
    def summary_ic_table(self, kind, start=False, end=False):
        """
        计算IC和分组IC的样本统计量
        对于ic的综合描述，包括均值，ir，显著性，偏度，峰度
        :param start: 开始日期，如’2021-01-01‘
        :param end: 结束日期，如’2022-01-01‘
        :param kind: 计算ic的方式，默认是spearman
        :return: 两个dataframe
        """
        if not start:
            start = self.factor.index[0]
        if not end:
            end = self.factor.index[-1]
        summary_ic = pd.DataFrame()
        summary_group_ic = pd.DataFrame()
        for result in self.results:
            adj_period = result['adj_period']  # 取周期=adj_period的数据
            # ==============================
            # IC的分析
            # ==============================
            ic = result['ic'].copy()
            ic = ic[(ic.index >= start) & (ic.index <= end)]
            # print('\nresult',result,'\nadj_period',adj_period,'\nic',ic)
            # 计算ic_mean
            ic_mean = ic.mean()
            ic_std = ic.std(ddof=1)
            # 计算ic_ir
            ic_ir = np.abs(ic_mean) / ic_std
            # 偏度
            ic_skew = ic.skew()
            # 峰度
            ic_kurtosis = ic.kurtosis()
            # 检验是否显著为0
            t_0 = np.sqrt(len(ic)) * ic_mean / ic_std
            p_0 = np.abs(t_0).apply(lambda x: (1 - scipy.stats.t.cdf(x, len(ic) - 1)) / 2)
            df_tmp1 = pd.DataFrame(data=[ic_mean, ic_ir,
                                         ic_skew, ic_kurtosis,
                                         t_0, p_0],
                                   index=['IC Mean', 'IC IR',
                                          'IC Skew', 'IC Kurtosis',
                                          't: IC!=0', 'p: IC!=0'])
            df_tmp1.columns = [[f'period_{adj_period}', f'period_{adj_period}'],
                               ['IC', 'rank IC']]
            # # 检验是否显著大于0.03
            # t_003 = np.sqrt(len(ic)) * (ic_mean - 0.03) / ic_std
            # p_003 = np.abs(t_003).apply(lambda x: 1-scipy.stats.t.cdf(x, len(ic)-1))
            # # 检验是否显著小于-0.03
            # t_003_ = np.sqrt(len(ic)) * (ic_mean + 0.03) / ic_std
            # p_003_ = np.abs(t_003_).apply(lambda x: 1-scipy.stats.t.cdf(x, len(ic)-1))
            # df_tmp1 = pd.DataFrame(data=[ic_mean, ic_ir, ic_skew, ic_kurtosis,
            #                              t_0, p_0,
            #                              t_003, p_003,
            #                              t_003_, p_003_],
            #                        index=['IC Mean', 'IC IR', 'IC Skew', 'IC Kurtosis',
            #                               't: IC!=0', 'p: IC!=0',
            #                               't: IC>0.03', 'p: IC>0.03',
            #                               't: IC<-0.03', 'p: IC<-0.03'])
            # df_tmp1.columns = [[f'period_{adj_period}', f'period_{adj_period}'],
            #                    ['IC', 'rank IC']]
            df_tmp1 = df_tmp1.round(4)
            summary_ic = pd.concat([summary_ic, df_tmp1], axis=1)

            # ==============================
            # Group_IC的分析
            # ==============================
            group_ic = result['group_ic'].copy()
            group_ic = group_ic[(group_ic.index >= start) & (group_ic.index <= end)]
            # print('\nresult',result,'\nadj_period',adj_period,'\ngroup_ic',group_ic)

            # 计算group_ic_mean
            group_ic_mean = group_ic.mean()
            group_ic_std = group_ic.std(ddof=1)
            # 计算group_ic_ir
            group_ic_ir = group_ic_mean / group_ic_std
            # 偏度
            group_ic_skew = group_ic.skew()
            # 峰度
            group_ic_kurtosis = group_ic.kurtosis()
            # 检验是否显著为0
            t_0 = group_ic_ir * np.sqrt(len(group_ic))
            p_0 = np.abs(t_0).apply(lambda x: (1 - scipy.stats.t.cdf(x, len(ic) - 1)) / 2)
            df_tmp2 = pd.DataFrame(data=[group_ic_mean, group_ic_ir,
                                         group_ic_skew, group_ic_kurtosis,
                                         t_0, p_0],
                                   index=['IC Mean', 'IC IR', 'IC Skew', 'IC Kurtosis',
                                          't: IC!=0', 'p: IC!=0'])
            df_tmp2.columns = [f'period_{adj_period}: group rank IC']
            df_tmp2 = df_tmp2.round(4)
            summary_group_ic = pd.concat([summary_group_ic, df_tmp2], axis=1)

        dict1 = {}
        if kind == 'rank_ic':
            df_ic = summary_ic.iloc[:, list(np.arange(0, 2 * len(self.results), 2) + 1)]
            df_ic.columns = [f'{x[0]}: {x[1]}' for x in df_ic.columns]
            dict1['ic'] = df_ic
            dict1['group_ic'] = summary_group_ic
            return dict1
        elif kind == 'ic':
            df_ic = summary_ic.iloc[:, list(np.arange(0, 2 * len(self.results), 2))]
            df_ic.columns = [f'{x[0]}: {x[1]}' for x in df_ic.columns]
            dict1['ic'] = df_ic
            dict1['group_ic'] = summary_group_ic
            return dict1

    def plot_cum_ic(self, kind):
        """
        画ic的累计变化曲线图
        :param kind:
        :return:
        """
        fig = plt.figure(figsize=(10, 5 * len(self.results)))
        # print(len(self.results))
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']  # list的第i个字典中key = adj_period的数据值
            ic = self.results[i]['ic'][kind].to_frame()  # list的第i个字典中key = IC 的数据框，就一行
            ic.index = pd.to_datetime(ic.index)
            ic.sort_index(ascending=True, axis=1, inplace=True)
            ic['cum'] = np.cumsum(ic[kind])

            ax = fig.add_subplot(100 * (len(self.results)) + 10 + (i + 1))
            ax.plot(ic.index, ic['cum'], alpha=1, color='C0')
            ax.set_title(f'Adj_periods_{adj_period}: {kind}', fontsize=15)
            ax.legend(['cumulative'], loc='upper left')
        plt.tight_layout()
        plt.show()


    def plot_ic_bar(self, kind, frequency='Y'):
        """
        画ic的变化柱状图，可以按年或者按月
        :param kind:
        :param frequency:
        :return:
        """
        df_ic = pd.DataFrame()
        for result in self.results:
            ic = result['ic'].loc[:, kind].copy()
            ic = ic.resample(frequency, axis=0, label='right',
                             closed='right', kind='period').mean().to_frame()
            ic = (100 * ic).round(2)
            ic.columns = [result['adj_period']]
            df_ic = pd.concat([df_ic, ic], axis=1)
        df_ic.plot(kind='bar', figsize=(10, 5))  # edgecolor='white', linewidth=5
        plt.title('IC (%)', fontsize=15)
        plt.legend(fontsize=15)
        plt.show()

    #
    def plot_daily_ic(self, kind):
        """
        画多空收益率按30天滚动和每日的IC
        在legend中，plot的优先级高于bar
        :param kind:
        :return:
        """
        fig = plt.figure(figsize=(10, 5 * len(self.results)))
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            ic = self.results[i]['ic'][kind].to_frame()
            ic = (100 * ic).round(2)
            ic['30d'] = ic[kind].rolling(window=int(20 // adj_period) + 1).mean()
            ic.index = pd.to_datetime(ic.index)
            ic.sort_index(ascending=True, axis=1, inplace=True)
            ax = fig.add_subplot(100 * (len(self.results)) + 10 + (i + 1))
            ax.bar(ic.index, ic[kind], alpha=0.6, width=2)
            ax.plot(ic.index, ic['30d'], alpha=1, color='red')
            ax.set_title(f'Adj_periods_{adj_period}: {kind} (%)', fontsize=15)
            ax.legend(['30d', 'daily'], loc='upper left')  # 先写plot的，再写bar的
            ax.axhline(3, color='black', linestyle='--')
            ax.axhline(-3, color='black', linestyle='--')
            ax.fill_between(ic.index, 3, -3, color='yellow', alpha=0.4)
            ax.set_ylim(-40, 40)
        plt.tight_layout()
        plt.show()

    def plot_monthly_ic(self, kind):
        """
        :param kind:
        :return:
        """
        plt.style.use('default')
        fig = plt.figure(figsize=(10, 5 * len(self.results)))
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            ic = self.results[i]['ic'][kind].to_frame()
            ic = ic.resample('M', axis=0, label='right',
                             closed='right', kind='period').mean()
            ic.columns = [kind]
            ic['year'] = ic.index.year
            ic['month'] = ic.index.month
            ic = ic.pivot_table(index=['year'], columns=['month'],
                                values=[kind])
            ic.columns = [x[1] for x in ic.columns]
            ic = (100 * ic).round(2)
            ax = fig.add_subplot(100 * len(self.results) + 10 + (i + 1))
            ax.imshow(ic.values, cmap="summer")

            # Rotate the tick labels and set their alignment.==============================================================
            ax.set_xticks(np.arange(len(ic.columns)))
            ax.set_xticklabels(ic.columns, fontsize=15)

            ax.set_yticks(np.arange(len(ic.index)))
            ax.set_yticklabels(ic.index, fontsize=15)

            # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            #          rotation_mode="anchor")
            for j in range(len(ic.index)):
                for k in range(len(ic.columns)):
                    text = ax.text(k, j, ic.values[j, k],
                                   ha="center", va="center", color="black")
            ax.set_title(f'Adj_period_{adj_period}: Monthly IC (%)')
        plt.tight_layout()
        plt.show()
        # plt.style.use('_mpl-gallery')

    # ==============================

    # 做分层收益的表格
    def summary_layer_ret(self, excess=False, long=True):
        summary_layer_ret_table = pd.DataFrame()
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            rf = self.rf
            ret = self.results[i]['ret'].T.copy()  # 转置了，索引为调仓日期，组号为列名
            exchange = self.results[i]['exchange'].T.copy()
            if excess:
                ret = ret - ret.mean(axis=1).values.reshape(len(ret), 1)
            ret = ret * (long * 2 - 1)  # long就是不变，short就是乘以-1
            s_ret = ret # ret.iloc[:, [0, 1, -2, -1]]  # 选取首尾几列
            s_exchange = exchange #exchange.iloc[:, [0, 1, -2, -1]]
            for j in s_ret.columns:
                ret_sub = s_ret.loc[:, j]
                exchange_sub = s_exchange.loc[:, j]

                AVolatility = np.std(ret_sub) * np.sqrt(252 / adj_period)
                WinningRatio = len(ret_sub[ret_sub > 0]) / len(ret_sub[ret_sub != 0])
                PnLRatio = np.mean(ret_sub[ret_sub > 0]) / abs(np.mean(ret_sub[ret_sub < 0]))
                ret_sub = np.cumprod(1 + ret_sub, axis=0)
                AReturnRate = (ret_sub.iloc[-1] / ret_sub.iloc[0]) ** (1 / (len(ret_sub) * adj_period / 252)) - 1
                SharpeRatio = (AReturnRate - rf) / AVolatility
                ExchangeMean = np.mean(exchange_sub)

                ret_sub = ret_sub.to_list()
                low_point = np.argmax((np.maximum.accumulate(ret_sub) - ret_sub) / np.maximum.accumulate(ret_sub))
                if low_point == 0:
                    MaxDrawdown = 0
                else:
                    high_point = np.argmax(ret_sub[:low_point])
                    MaxDrawdown = (ret_sub[high_point] - ret_sub[low_point]) / ret_sub[high_point]

                df_tmp = pd.DataFrame(data=[AReturnRate, AVolatility,
                                            SharpeRatio, MaxDrawdown,
                                            WinningRatio, PnLRatio, ExchangeMean],
                                      index=['AReturnRate', 'AVolatility',
                                             'SharpeRatio', 'MaxDrawdown',
                                             'WinningRatio', 'PnLRatio', 'ExchangeMean'],
                                      columns=[f'period{adj_period} group{j}'])
                summary_layer_ret_table = pd.concat([summary_layer_ret_table, df_tmp], axis=1)
        return summary_layer_ret_table  # 包含不同周期设置下的数据框

    # 画每一组的收益率
    def plot_layer_ret_bar(self, excess=True, long=True):
        df_ret = pd.DataFrame()
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            ret = self.results[i]['ret'].T.copy()
            ret.columns = list(range(1, self.n_group + 1))
            ret.index = pd.to_datetime(ret.index)
            if excess:
                ret = ret - ret.mean(axis=1).values.reshape(len(ret), 1)
            ret = np.cumprod(1 + ret * (long * 2 - 1), axis=0) - 1
            ret = (1 + ret) ** (1 / (len(ret) * adj_period)) - 1
            # ret.dropna(axis=0, inplace=True)
            ret = 10000 * ret.iloc[-1, :].to_frame()
            ret.columns = [adj_period]
            df_ret = pd.concat([df_ret, ret], axis=1)
        df_ret.plot(kind='bar', figsize=(10, 5))
        plt.title('Group Return', fontsize=15)
        plt.ylabel('period return (bps)', fontsize=15)
        plt.legend(fontsize=15)
        plt.show()

    # 得到每一组的收益率表格
    def layer_ret_df(self):
        self.calc_multiple_periods()

        df_ret = pd.DataFrame()
        for i in range(len(self.results)):
            ret = self.results[i]['ret'].T.copy()
            ret.columns = list(range(1, self.n_group + 1))
            ret.index = pd.to_datetime(ret.index)
            df_ret = pd.concat([df_ret, ret], axis=1)
        return df_ret

    # 画分层收益率的图
    def plot_layer_ret(self, excess=True, long=True):
        cmap = cm.get_cmap("RdYlGn")
        cmap = cmap(np.linspace(0, 1, self.n_group))
        fig = plt.figure(figsize=(10, 5 * len(self.results)))

        for i in range(len(self.results)):
            ret = self.results[i]['ret'].T.copy()
            if excess:
                ret = ret - ret.mean(axis=1).values.reshape(len(ret), 1)
            ret = np.cumprod(1 + ret * (long * 2 - 1), axis=0)
            ax = fig.add_subplot(100 * len(self.results) + 10 + (i + 1))
            for j in range(len(ret.columns)):
                ax.plot(ret.index, ret.iloc[:, j].values, color=cmap[j], alpha=1)
            ax.legend(ret.columns, loc='upper left', fontsize=10)
            adj_period = self.results[i]['adj_period']
            ax.set_title(f'Adj_periods_{adj_period}: Net Value for Groups', fontsize=15)
            ax.axhline(1, linestyle='--', c='grey')
        plt.tight_layout()
        plt.show()

    # 多空组合--------------------------------------------------------------------------------------------------------------------

    # 做多空收益组合的表格
    def summary_long_short_ret(self, k):
        summary_long_short_table = pd.DataFrame()
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            rf = self.rf
            ret = self.results[i]['ret'].T.copy()
            if k == 1:
                ret = ret.iloc[:, -1] - ret.iloc[:, 0]
            elif k == 2:  # 买卖第2和倒数第2层
                ret = ret.iloc[:, -2] - ret.iloc[:, 1]

            AVolatility = np.std(ret) * np.sqrt(252 / adj_period)
            WinningRatio = len(ret[ret > 0]) / len(ret[ret != 0])
            PnLRatio = np.mean(ret[ret > 0]) / abs(np.mean(ret[ret < 0]))
            ret = np.cumprod(1 + ret, axis=0)
            AReturnRate = (ret.iloc[-1] / ret.iloc[0]) ** (1 / (len(ret) * adj_period / 252)) - 1
            SharpeRatio = (AReturnRate - rf) / AVolatility

            ret = ret.to_list()
            low_point = np.argmax((np.maximum.accumulate(ret) - ret) / np.maximum.accumulate(ret))
            if low_point == 0:
                MaxDrawdown = 0
            else:
                high_point = np.argmax(ret[:low_point])
                MaxDrawdown = (ret[high_point] - ret[low_point]) / ret[high_point]

            df_tmp = pd.DataFrame(data=[AReturnRate, AVolatility,
                                        SharpeRatio, MaxDrawdown,
                                        WinningRatio, PnLRatio],
                                  index=['AReturnRate', 'AVolatility',
                                         'SharpeRatio', 'MaxDrawdown',
                                         'WinningRatio', 'PnLRatio'],
                                  columns=[f'period{adj_period}'])

            summary_long_short_table = pd.concat([summary_long_short_table, df_tmp], axis=1)
        return summary_long_short_table

    # 画多空收益组合的图
    def plot_long_short_ret(self, k):
        fig = plt.figure(figsize=(10, 5 * len(self.results)))
        for i in range(len(self.results)):
            ret = self.results[i]['ret'].T.copy()
            if k == 1:
                ret = np.cumprod(1 + (ret.iloc[:, -1] - ret.iloc[:, 0]), axis=0).to_frame(name='ret')
            elif k == 2:  # 买卖第2和倒数第2层
                ret = np.cumprod(1 + (ret.iloc[:, -2] - ret.iloc[:, 1]), axis=0).to_frame(name='ret')

            ret['ADD'] = -(np.maximum.accumulate(ret.ret) - ret.ret) / np.maximum.accumulate(ret.ret)

            ax1 = fig.add_subplot(100 * len(self.results) + 10 + (i + 1))
            ax1.plot(ret.index, ret.loc[:, 'ret'], color='C0', alpha=1)
            ax1.legend(['Net_Value'], loc='upper left', fontsize=10)
            ax1.axhline(1, linestyle='--', c='grey')
            ax2 = ax1.twinx()
            ax2.fill_between(ret.index, 0, ret.loc[:, 'ADD'], color='red', alpha=0.5)
            ax2.set_ylim(-0.5, 0)
            ax2.legend(['Accumulated_Drawdown'], loc='lower right', fontsize=10)
            adj_period = self.results[i]['adj_period']
            ax2.set_title(f'Adj_periods_{adj_period}: Long Short Portfolio Net Value', fontsize=15)
            ax2.grid()

        plt.tight_layout()
        plt.show()

    # 收益率分析
    # ==============================
    # plot_monthly
    def plot_monthly_ret(self, k):  # k会和下面的循环局部变量重复，修改名称
        a = k
        plt.style.use('default')
        fig = plt.figure(figsize=(10, 5 * len(self.results)))
        # print(self.results)
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            ret = self.results[i]['ret'].T.copy()

            # ret = (ret.iloc[:, -1] - ret.iloc[:, 0]).to_frame()  # 用普通收益率算多空
            if a == 1:
                ret = (ret.iloc[:, -1] - ret.iloc[:, 0]).to_frame()  # 用普通收益率算多空
            elif a == 2:  # 买卖第2和倒数第2层
                ret = (ret.iloc[:, -2] - ret.iloc[:, 1]).to_frame()
            ret = np.log(1 + ret)  # 转化为对数收益率方便加和
            ret = ret.resample('M', axis=0, label='right',
                               closed='right', kind='period').sum()

            # print('\nfff\n',ret)
            ret = np.exp(ret) - 1  # 计算出了每个月的普通收益率
            ret.columns = ['ret']
            ret['year'] = ret.index.year
            ret['month'] = ret.index.month
            ret = ret.pivot_table(index=['year'], columns=['month'],
                                  values=['ret'])
            ret.columns = [x[1] for x in ret.columns]
            ret = (100 * ret).round(2)
            ax = fig.add_subplot(100 * len(self.results) + 10 + (i + 1))
            ax.imshow(ret.values, cmap="summer")  # YlGn

            # Rotate the tick labels and set their alignment.
            ax.set_xticks(np.arange(len(ret.columns)))
            ax.set_xticklabels(ret.columns, fontsize=15)

            ax.set_yticks(np.arange(len(ret.index)))
            ax.set_yticklabels(ret.index, fontsize=15)

            # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            #          rotation_mode="anchor")

            for j in range(len(ret.index)):
                for k in range(len(ret.columns)):
                    text = ax.text(k, j, ret.values[j, k],
                                   ha="center", va="center", color="black")
            ax.set_title(f'Adj_period_{adj_period}: Long Short Monthly Return (%)')

        plt.tight_layout()
        plt.show()
        # plt.style.use('_mpl-gallery')

    # 画多空收益率按30天滚动和每日的收益率
    def plot_daily_ret(self, k):
        fig = plt.figure(figsize=(10, 5 * len(self.results)))
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            ret = self.results[i]['ret'].T.copy()
            if k == 1:
                ret = (ret.iloc[:, -1] - ret.iloc[:, 0]).to_frame()
            elif k == 2:  # 买卖第2和倒数第2层
                ret = (ret.iloc[:, -2] - ret.iloc[:, 1]).to_frame()

            ret.columns = ['daily']
            ret = (100 * ret).round(2)
            ret['30d'] = ret['daily'].rolling(window=(1 + 20 // adj_period)).mean()
            ax = fig.add_subplot(100 * len(self.results) + 10 + (i + 1))
            ax.bar(ret.index, ret['daily'], alpha=0.6, width=2)
            ax.plot(ret.index, ret['30d'], alpha=1, color='red')
            ax.legend(['30d', 'daily'], loc='upper left')  # 先写plot的，再写bar的
            adj_period = self.results[i]['adj_period']
            ax.set_title(f'Adj_periods_{adj_period}: Long Short Portfolio Returns', fontsize=15)
            # axes[i].axhline(0, linestyle='--', c='grey')
            ax.set_ylabel('return (%)')
        plt.tight_layout()
        plt.show()

    # 画表现最好一组的按30天滚动和每日的换手率
    # 在legend中，plot的优先级高于bar
    def plot_daily_exchange(self):
        fig = plt.figure(figsize=(10, 5*len(self.results)))
        for i in range(len(self.results)):
            adj_period = self.results[i]['adj_period']
            exchange = self.results[i]['exchange'].sort_index(ascending=True).iloc[-1,:].T.to_frame('daily')
            exchange = (100 * exchange).round(2)
            smooth = int(20//adj_period)+1
            exchange['30d'] = exchange.rolling(window=smooth).mean()
            exchange.index = pd.to_datetime(exchange.index)
            exchange.sort_index(ascending=True, axis=1, inplace=True)
            ax = fig.add_subplot(100*(len(self.results)) + 10 + (i+1))
            mean_ex = round(np.mean(exchange['daily']),2)
            ax.axhline(mean_ex,color='k',linestyle='--')
            ax.bar(exchange.index, exchange['daily'], alpha=0.6, width=2)
            ax.plot(exchange.index, exchange['30d'], alpha=1, color='red')
            ax.set_title(f'Adj_periods_{adj_period}: exchange_rate (%)', fontsize=15)
            ax.legend([f'mean:{mean_ex}%','30d', 'daily'], loc='upper left')  # 先写plot的，再写bar的
            up = min(mean_ex + 2 * np.std(exchange['daily']),100)
            down = mean_ex - 2 * np.std(exchange['daily'])
            # ax.fill_between(exchange.index, np.mean(exchange['daily']), 0,color='yellow',alpha=0.4)
            ax.set_xlim(exchange.index[0], exchange.index[-1])
            ax.set_ylim(down, up)
        plt.tight_layout()
        plt.show()

    # ==============================
    # ==============================
    # 快速分析
    # ==============================
    # ==============================
    def Long_Short_analysis(self, k, rating=False):
        self.calc_multiple_periods()

        # 多空组合测试
        print("{:=^120s}".format('Return'))
        print("\n{:-^120s}".format('Long Short performance'))
        summary_long_short_ret = self.summary_long_short_ret(k=k)
        print(summary_long_short_ret)

        print("\n{:-^120s}".format('Long Short Return'))
        self.plot_long_short_ret(k=k)

        print("\n{:-^120s}".format('Long Short Monthly Return'))
        self.plot_monthly_ret(k=k)

        print("\n{:-^120s}".format('Long Short Daily Return'))
        self.plot_daily_ret(k=k)

    def Long_analysis(self, rating=False):
        self.calc_multiple_periods()

        print("\n{:-^120s}".format('Long Returns Performance'))
        self.plot_layer_ret_bar(excess=False, long=True)
        summary_layer_ret = self.summary_layer_ret(excess=False, long=True)
        print(summary_layer_ret)
        self.plot_layer_ret(excess=False, long=True)

        print("\n{:-^120s}".format('Excess Long Returns Performance'))
        self.plot_layer_ret_bar(excess=True, long=True)
        summary_layer_ret = self.summary_layer_ret(excess=True, long=True)
        print(summary_layer_ret)
        self.plot_layer_ret(excess=True, long=True)

        print("\n{:-^120s}".format('Exchange_rate of Best Group'))
        self.plot_daily_exchange()

    def Short_analysis(self, rating=False):
        self.calc_multiple_periods()

        print("\n{:-^120s}".format('Short Returns Performance'))
        self.plot_layer_ret_bar(excess=False, long=False)
        summary_layer_ret = self.summary_layer_ret(excess=False, long=False)
        print(summary_layer_ret)
        self.plot_layer_ret(excess=False, long=False)

        print("\n{:-^120s}".format('Excess Short Returns Performance'))
        self.plot_layer_ret_bar(excess=True, long=False)
        summary_layer_ret = self.summary_layer_ret(excess=True, long=False)
        print(summary_layer_ret)
        self.plot_layer_ret(excess=True, long=False)

    def IC_analysis(self, rating=False):
        self.calc_multiple_periods()
        # ic的部分
        print("{:=^120s}".format('IC'))
        summary_ic_table = self.summary_ic_table(kind='ic')
        print("\n{:-^120s}".format('IC performance'))
        print(summary_ic_table['ic'])
        print("\n{:-^120s}".format('IC yearly'))
        self.plot_ic_bar(kind='ic', frequency='Y')
        print("\n{:-^120s}".format('IC monthly'))
        self.plot_monthly_ic(kind='ic')
        print("\n{:-^120s}".format('IC daily'))
        self.plot_daily_ic(kind='ic')
        print("\n{:-^120s}".format('Cumulative IC'))
        self.plot_cum_ic(kind='ic')

    def Rank_IC_analysis(self, rating=False):
        self.calc_multiple_periods()

        # rank_ic的部分
        print("{:=^120s}".format('IC'))
        summary_ic_table = self.summary_ic_table(kind='rank_ic')
        print("\n{:-^120s}".format('IC performance'))
        print(summary_ic_table['ic'])
        print('\nGroup IC performance: ')
        print(summary_ic_table['group_ic'])
        print("\n{:-^120s}".format('IC yearly'))
        self.plot_ic_bar(kind='rank_ic', frequency='Y')
        print("\n{:-^120s}".format('IC monthly'))
        self.plot_monthly_ic(kind='rank_ic')
        print("\n{:-^120s}".format('IC daily'))
        self.plot_daily_ic(kind='rank_ic')
        print("\n{:-^120s}".format('Cumulative IC'))
        self.plot_cum_ic(kind='rank_ic')

    def test_result_table(self, rating=False):
        self.calc_multiple_periods()

        long_short_table_1 = self.summary_long_short_ret(k=1)
        long_short_table_2 = self.summary_long_short_ret(k=2)

        long_table = self.summary_layer_ret(excess=False, long=True)
        long_excess_table = self.summary_layer_ret(excess=True, long=True)

        short_table = self.summary_layer_ret(excess=False, long=False)  # 数据框
        short_excess_table = self.summary_layer_ret(excess=True, long=False)

        ic_table = self.summary_ic_table(kind='ic')  # 字典
        rank_ic_table = self.summary_ic_table(kind='rank_ic')  # 字典

        return long_short_table_1, long_short_table_2, long_table, long_excess_table, short_table, short_excess_table, ic_table, rank_ic_table

    def model_rating(self):
        self.calc_multiple_periods()
        summary_layer_ret = self.summary_layer_ret(excess=False, long=True)
        summary_ic_table = self.summary_ic_table()
        score = summary_layer_ret.iloc[2, 1] + summary_ic_table.iloc[0, :].mean()

        return score

    """
    多因子集中测试，使用的输出函数
    集中输出因子的测试和统计指标
    在因子构造、factor3_test中被调用
    24.1.13
    """
    def statistics_result_df(self, kind,rating=False):  #调仓周期必须包含1,分层数量改变没事
        self.calc_multiple_periods()
        n = self.list_periods[0]

        ic = self.results[0]['ic'][kind].to_frame()  # list的第i个字典中key = IC 的数据框中的kind = 'ic'列，就一行
        ic.index = pd.to_datetime(ic.index)
        ic.sort_index(ascending=True, axis=1, inplace=True)
        ic['cum'] = np.cumsum(ic[kind])


        ret = self.results[0]['ret'].T.copy()  # 转置了，索引为调仓日期，组号为列名

        ret_excess = ret - ret.mean(axis=1).values.reshape(len(ret), 1)  # 超额多头
        s_ret = ret_excess.iloc[:, -2]  # 选取倒数第2列
        L_excess_cum_returns = (1 + s_ret).cumprod() - 1  # cum_returns

        ret_LS = ret.iloc[:, -2] - ret.iloc[:, 1]  # 买卖第2和倒数第2层
        LS_cum_returns = (1 + ret_LS).cumprod() - 1  # cum_returns

        reg = self.results[0]['reg'].copy()  #results是一个包含不同调仓周期下字典的list，第一个字典中key = 'reg'的数据框，列名为['beta', 'intercept','r_value','p_value','std_err','t_value'],
        reg_beta_mean = reg['beta'].mean()
        reg_t_value_mean = reg['t_value'].mean()
        reg_p_value_mean = reg['p_value'].mean()



        long_short_table_1 = self.summary_long_short_ret(k=1)
        long_short_table_2 = self.summary_long_short_ret(k=2)

        long_table = self.summary_layer_ret(excess=False, long=True)
        long_excess_table = self.summary_layer_ret(excess=True, long=True)

        short_table = self.summary_layer_ret(excess=False, long=False)  # 数据框
        short_excess_table = self.summary_layer_ret(excess=True, long=False)

        ic_table = self.summary_ic_table(kind='ic')  # 字典
        rank_ic_table = self.summary_ic_table(kind='rank_ic')  # 字典

        # 提取值并存储为变量
        p1_ic_mean = ic_table['ic'].loc['IC Mean', 'period_1: IC']
        p1_ic_ir = ic_table['ic'].loc['IC IR', 'period_1: IC']
        p1_ic_t_value = ic_table['ic'].loc['t: IC!=0', 'period_1: IC']
        p1_ic_p_value = ic_table['ic'].loc['p: IC!=0', 'period_1: IC']

        p1_group_rank_ic_mean = ic_table['group_ic'].loc['IC Mean', 'period_1: group rank IC']
        p1_group_rank_ic_ir = ic_table['group_ic'].loc['IC IR', 'period_1: group rank IC']
        p1_group_rank_ic_t_value = ic_table['group_ic'].loc['t: IC!=0', 'period_1: group rank IC']
        p1_group_rank_ic_p_value = ic_table['group_ic'].loc['p: IC!=0', 'period_1: group rank IC']

        p1_rank_ic_mean = rank_ic_table['ic'].loc['IC Mean', 'period_1: rank IC']
        p1_rank_ic_ir = rank_ic_table['ic'].loc['IC IR', 'period_1: rank IC']
        p1_rank_ic_t_value = rank_ic_table['ic'].loc['t: IC!=0', 'period_1: rank IC']
        p1_rank_ic_p_value = rank_ic_table['ic'].loc['p: IC!=0', 'period_1: rank IC']

        p1_return_1 = long_short_table_1.loc['AReturnRate', 'period1']
        p1_sharpe_table_1 = long_short_table_1.loc['SharpeRatio', 'period1']

        p1_return_2 = long_short_table_2.loc['AReturnRate', 'period1']
        p1_sharpe_2 = long_short_table_2.loc['SharpeRatio', 'period1']

        p1_g9_return = long_excess_table.loc['AReturnRate', f'period1 group{self.n_group-1}']
        p1_g9_sharpe = long_excess_table.loc['SharpeRatio', f'period1 group{self.n_group-1}']

        p1_g10_excess_return = long_excess_table.loc['AReturnRate',f'period1 group{self.n_group}']
        p1_g10_excess_sharpe = long_excess_table.loc['SharpeRatio',f'period1 group{self.n_group}']

        # 将所有变量值放入一个字典中，其中键为变量名，值为变量值
        data = {
            'p1_ic_mean': [p1_ic_mean],
            'p1_ic_ir': [p1_ic_ir],
            'p1_ic_t_value': [p1_ic_t_value],

            'p1_rank_ic_mean': [p1_rank_ic_mean],
            'p1_rank_ic_ir': [p1_rank_ic_ir],
            'p1_rank_ic_t_value': [p1_rank_ic_t_value],

            'reg_beta_mean': reg_beta_mean,
            'reg_t_value_mean': reg_t_value_mean,

            'p1_group_rank_ic_mean': [p1_group_rank_ic_mean],
            'p1_group_rank_ic_ir': [p1_group_rank_ic_ir],
            'p1_group_rank_ic_t_value': [p1_group_rank_ic_t_value],

            'p1_ls_return_1': [p1_return_1],
            'p1_ls_sharpe_1': [p1_sharpe_table_1],
            'p1_ls_return_2': [p1_return_2],
            'p1_ls_sharpe_2': [p1_sharpe_2],

            'p1_g9_l_excess_return': [p1_g9_return],
            'p1_g9_l_excess_sharpe': [p1_g9_sharpe],
            'p1_g10_l_excess_return': [p1_g10_excess_return],
            'p1_g10_l_excess_sharpe': [p1_g10_excess_sharpe],

            'p1_ic_p_value': [p1_ic_p_value],
            'p1_rank_ic_p_value': [p1_rank_ic_p_value],
            'p1_group_rank_ic_p_value': [p1_group_rank_ic_p_value],
            'reg_p_value_mean': reg_p_value_mean,

        }

        # 从字典创建数据框
        df = pd.DataFrame(data)

        return df, ic['cum'], L_excess_cum_returns, LS_cum_returns

    """
    想直接集中输出策略，集中调参，但似乎不行；
    目前没用
    24.1.13
    """
    def factor_strategy(self):
        self.calc_multiple_periods()


        empty_df = pd.DataFrame()
        empty_df['long'] = np.nan
        empty_df['long_excess'] = np.nan


        long_table = self.summary_layer_ret(excess=False, long=True)
        long_excess_table = self.summary_layer_ret(excess=True, long=True)
        g10_return = long_table.iloc[:, -1].T
        g10_excess_return = long_excess_table.iloc[:, -1].T
        strategy_10_dict ={
            'factor': [self.factor],
            'list_periods': [self.list_periods],
            'asset_weight': [self.asset_weight],
            'n_group': [self.n_group],
            'group_NO.': [1],
        }

        strategy_10_df  =  pd.DataFrame(strategy_10_dict)
        strategy_10_df = pd.concat([strategy_10_df,empty_df['long'],g10_return,empty_df['long_excess'],g10_excess_return], axis=1)

        g9_return = long_table.iloc[:, -2].T
        g9_excess_return = long_excess_table.iloc[:, -2].T

        strategy_9_dict ={
            'factor': [self.factor],
            'list_periods': [self.list_periods],
            'asset_weight': [self.asset_weight],
            'n_group': [self.n_group],
            'group_NO.': [2],
        }
        strategy_9_df  =  pd.DataFrame(strategy_9_dict)
        strategy_9_df = pd.concat([strategy_9_df,empty_df['long'],g9_return,empty_df['long_excess'],g9_excess_return], axis=1)

        strategy_df = pd.concat( [strategy_10_df , strategy_9_df], axis=0)



        i = 0
        rf = self.rf
        ret = self.results[i]['ret'].T.copy()  # 转置了，索引为调仓日期，组号为列名

        L_ret = pd.DataFrame()
        L_ret[f'@{self.factor}@{self.list_periods}@{self.asset_weight}@{self.n_group}@gruop_NO.1@Long_ret'] \
            = (1 + ret.iloc[:, -1] ).cumprod() - 1
        L_ret[f'@{self.factor}@{self.list_periods}@{self.asset_weight}@{self.n_group}@gruop_NO.2@Long_ret'] \
            = (1 + ret.iloc[:, -2] ).cumprod() - 1



        L_excess_ret = pd.DataFrame()
        ret_excess = ret - ret.mean(axis=1).values.reshape(len(ret), 1)  # 超额多头
        s_ret_10 = ret_excess.iloc[:, -1]  # 选取倒数第2列
        L_excess_ret[f'@{self.factor}@{self.list_periods}@{self.asset_weight}@{self.n_group}@gruop_NO.1@Long_excess_ret'] \
            = (1 + s_ret_10).cumprod() - 1  # cum_returns

        ret_excess = ret - ret.mean(axis=1).values.reshape(len(ret), 1)  # 超额多头
        s_ret_9 = ret_excess.iloc[:, -2]  # 选取倒数第2列
        L_excess_ret[f'@{self.factor}@{self.list_periods}@{self.asset_weight}@{self.n_group}@gruop_NO.2@Long_excess_ret'] \
            = (1 + s_ret_9).cumprod() - 1  # cum_returns

        return strategy_df, L_ret, L_excess_ret
        #输出两个数据框合并后的，一个9组，一个10组


