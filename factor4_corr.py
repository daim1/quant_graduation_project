import pandas as pd
import numpy as np
from Factor0_Analysis_fuc import *
import factor3_test as ftest

"""单因子回归，计算不同因子收益率的相关性矩阵"""
def beta_corr(dfs , ret):   #输入带时间索引,索引名称为因子名称的原因子数据框，带时间索引的原收益率数据框
    beta_df_list = []
    for df in dfs:
        df_factor, df_ret, N, factor_name = ftest.format_processing(df, ret)
        fa = FactorAnalysis(df_factor, df_ret, [1], 10,asset_weight = 'mean')   # 周期要写出list形式
        print(f"计算因子收益率beta：N={N}", factor_name)
        result = fa.calc_results(adj_periods=1)
        reg_df = result['reg'].copy()
        beta_df = reg_df['beta']
        beta_df_list.append(beta_df)
    combined_beta_df = pd.concat(beta_df_list, axis=1)
    combined_beta_df.columns = df_names
    beta_corr_matrix = combined_beta_df.corr().copy()  # 计算相关系数矩阵
    beta_corr_matrix.index.name = 'bate_corr' # 设置相关系数矩阵的行和列的名字
    beta_corr_matrix.columns.name = 'df_names'
    print(beta_corr_matrix)
    return beta_corr_matrix

"""计算不同因子之间，每行的相关系数矩阵"""
def factor_corr_df(dfs):
    correlation_matrices_dict = {}  #每天的相关性矩阵
    for index in dfs[0].index:  # 遍历第一个因子的每一天
        rows = []
        for df, name in zip(dfs, df_names):      # 提取每个DataFrame中对应的行并设置列名
            row = df.loc[index].to_frame(name)  #df.loc[index]变成一列，带着索引，然后命名
            rows.append(row)
        row_df = pd.concat(rows, axis=1)   #这一天的不同数据框的行，合并为多个列，列名为因子名称
        correlation_matrices_dict[index] = pd.DataFrame(row_df.corr()) # 计算相关性矩阵并将其添加到字典中

    combined_corr_dict = {}  # key是 配对因子名称，数据框是该配对因子，每天的相关性
    for i in range(len(df_names)):  # 遍历每一对因子
        for j in range(i+1, len(df_names)):   #不包含对角线的1
            combined_list = []          # 对于每一对因子，创建一个新的空列表用来存储对应位置的值
            for index in factor1.index:
                combined_list.append(correlation_matrices_dict[index].loc[df_names[i], df_names[j]])              # 提取每个相关性矩阵中对应位置的值，并将其添加到列表中
            combined_corr_dict[df_names[i] + "_vs_" + df_names[j]] = pd.DataFrame(combined_list, index=factor1.index, columns=[df_names[i] + "_" + df_names[j]])          # 创建新的 DataFrame，并将其添加到字典中
    combined_fac_corr_df = pd.concat(combined_corr_dict.values(), axis=1)  #列的名称为配对因子名称
    combined_fac_corr_df.columns = combined_corr_dict.keys()
    cum_corr_df = combined_fac_corr_df.apply(np.cumsum)   #列的名称为配对因子名称

    sum_df = pd.DataFrame()  # 创建一个空的 DataFrame 来存储总和
    count_df = pd.DataFrame()   # 记录每个位置的非空值的数量
    for beta_corr_matrix in correlation_matrices_dict.values():
        beta_corr_matrix = beta_corr_matrix.dropna()
        sum_df = sum_df.add(beta_corr_matrix, fill_value=0)
        count_df = count_df.add(beta_corr_matrix.notnull().astype(int), fill_value=0)
    average_df = sum_df.divide(count_df).fillna(0).copy()  # 计算均值
    average_df.index.name = 'factor_corr_avg'
    print(average_df)
    return average_df,cum_corr_df, combined_fac_corr_df


if __name__ == '__main__':
    print("==================================共线性分析==================================")


    """提供带索引的因子数据框list"""
    factor1 =  pd.read_excel('ret_auto_corr_tod_combined.xlsx', sheet_name='com_fac_mean')
    factor2 =  pd.read_excel('price_position_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor3 =  pd.read_excel('price_change_done.xlsx', sheet_name='past_5_price_change')
    factor4 =  pd.read_excel('ret_t_stat_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor5 =  pd.read_excel('price_slope_combined.xlsx', sheet_name='com_fac_mean')
    dfs = [factor1, factor2, factor3, factor4,factor5]
    df_names = ['ret_auto_corr', 'price_position', 'price_change','ret_t_stat','price_slope']
    for df in dfs:
        df.set_index(df.columns[0], inplace=True)  # 设置时间列为索引

    """提供单因子回归拟合目标，ret，或者ret_rank"""
    ret = pd.read_excel('中信三级行业_日频_因子.xlsx',
                        sheet_name='PCT_CHG').drop(columns=['CI005488', "CI005510", 'CI005566'])
    ret.set_index(ret.columns[0], inplace=True)
    ret = ret / 100

    """因子共线性分析,导出beta相关性矩阵、因子本身的相关性矩阵均值，配对因子的每期相关性序列、累计相关性序列"""
    beta_corr_matrix = beta_corr(dfs,ret)  #输入带时间索引,索引名称为因子名称的原因子数据框，带时间索引的原收益率数据框
    average_df,cum_corr_df,combined_fac_corr_df = factor_corr_df(dfs)  #同上
    with pd.ExcelWriter('毕设_因子共线性分析报表.xlsx') as writer:
        beta_corr_matrix.to_excel(writer, sheet_name='corr_matrix')
        startrow = beta_corr_matrix.shape[0] + 5
        average_df.to_excel(writer, sheet_name='corr_matrix', startrow=startrow)
        cum_corr_df.to_excel(writer, sheet_name='factor_cum_corr')
        combined_fac_corr_df.to_excel(writer, sheet_name='factor_corr')

    print("完成输出：毕设_因子共线性分析报表.xlsx")



