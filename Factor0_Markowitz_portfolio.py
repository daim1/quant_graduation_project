import pandas as pd
import numpy as np
from pandas import Series, DataFrame
#import tushare as ts
import scipy as sp
import scipy.optimize as scopt
import scipy.stats as spstats
import matplotlib.pyplot as plt
import numpy as np
from numpy import  linalg
import seaborn as sns
import scipy.optimize as scopt


def create_portfolio(tickers, weights=None):
    if weights is None:
        weights = np.ones(len(tickers)) / len(tickers)
    portfolio = pd.DataFrame({'Tickers': tickers,
                              'Weights': weights},
                             index=tickers)
    return portfolio


# 组合收益率函数
def calculate_weighted_portfolio_value(portfolio, returns, name='Value'):
    total_weights = portfolio.Weights.sum()
    weighted_returns = returns * (portfolio.Weights /
                                  total_weights)
    return pd.DataFrame({name: weighted_returns.sum(axis=1)})


def objfunvar(W, R, target_ret):
    cov = returns.cov()  # var-cov matrix
    port_var = np.dot(np.dot(W, cov), W.T)  # portfolio variance
    return np.sqrt(port_var)

def calc_efficient_frontier(returns, sellshort=False):
    """
    计算有效前沿，也就是最小方差以上的部分
    :param returns:
    :param sellshort:
    :return:{'Means': result_means,
            'Stds': result_stds,
            'Weights': result_weights,
            'Weights_change': results_weigths_change}
    """
    result_means = []
    result_stds = []
    result_weights = []
    results_weigths_change = []
    means = returns.mean()
    min_mean, max_mean = means.min(), means.max()
    nstocks = returns.columns.size
    if sellshort:
        bounds = None
    else:
        bounds = [(0, 1) for i in np.arange(nstocks)]
    for r in np.linspace(min_mean, max_mean, 100):
        weights = np.ones(nstocks) / nstocks
        constraints = ({'type': 'eq',
                        'fun': lambda W: np.sum(W) - 1},
                       {'type': 'eq',
                        'fun': lambda W: np.sum(W * means) - r})
        constraints1 = ({'type': 'eq',
                         'fun': lambda W: np.sum(W) - 1},
                        {'type': 'eq',
                         'fun': lambda W: np.sum(W * means) - r - 0.001})
        results = scopt.minimize(objfunvar, weights, (returns, r),
                                 method='SLSQP',
                                 constraints=constraints,
                                 bounds=bounds)
        if not results.success:  # handle error
            raise Exception(results.message)
        result_means.append(np.round(r, 6))  # 4 decimal places
        # std_=np.round(np.std(np.sum(returns*results.x,axis=1)),6)
        std_ = objfunvar(results.x, returns, r)
        result_stds.append(std_)
        result_weights.append(np.round(results.x, 5))
        if r + 0.001 < max_mean:
            results_change = scopt.minimize(objfunvar, weights, (returns, r + 0.001),
                                            method='SLSQP',
                                            constraints=constraints1,
                                            bounds=bounds)
            results_weigths_change.append(np.round(results_change.x - results.x, 5))
    return {'Means': result_means,
            'Stds': result_stds,
            'Weights': result_weights,
            'Weights_change': results_weigths_change}


def calc_portfolio_std(returns=None, sigma=None, weights=None):
    """
    计算加权后的资产组合标准差，作为分母
    """
    if weights is None:
        weights = np.ones(returns.columns.size) / \
                  returns.columns.size
    if returns is not None:
        sigma = returns.cov()
    var = weights.dot(sigma).dot(weights)
    return np.sqrt(var)


def negative_IC_ratio(weights, means, sig, risk_free_rate=0):
    """
    这是是特殊的，分子用IC的均值向量计算，不变
    分母用因子值的cov，不是IC的cov
    """
    # get the portfolio variance
    std = calc_portfolio_std(sigma=sig, weights=weights)
    # and return the sharpe ratio
    return -((means.dot(weights) - risk_free_rate) / (std))


def negative_sharpe_ratio(weights, means, sig, risk_free_rate=0):
    """
    计算加权后的资产组合 负夏普比率
    """
    # get the portfolio variance
    std = calc_portfolio_std(sigma=sig, weights=weights)
    # and return the sharpe ratio
    return -((means.dot(weights) - risk_free_rate) / (std))

def positive_std(weights, means, sig, risk_free_rate=0):
    """
    计算加权后的资产组合 标准差
    """
    # get the portfolio variance
    std = calc_portfolio_std(sigma=sig, weights=weights)
    return  std

def negative_return(weights, means, sig, risk_free_rate=0):
    """
    计算加权后的资产组合 收益率
    """
    return  -((means.dot(weights) - risk_free_rate))

def optimize_portfolio(returns, ret_cov = None, risk_free_rate=0, target_fun_name = 'max_sharpe', sellshort=False):
    """
    Performs the optimization，最大化夏普，最小化方差，最大化收益率
    :param returns:
    :param risk_free_rate:
    :param sellshort:
    :return: {'Means': mean_sharpe,
            'Stds': std_sharpe,
            'Weights': results.x}
    """
    means = returns.mean()
    if ret_cov is None:
        sig = returns.cov()
    else:
        sig = ret_cov.cov()
    weights = np.ones(returns.columns.size,  dtype=float) * 1.0 / returns.columns.size

    if sellshort:
        bounds = None
    else:
        bounds = [(0, 1) for i in np.arange(returns.columns.size)] #限制每个权重的取值区间

    constraints = ({'type': 'eq',
                    'fun': lambda W: np.sum(W) - 1})  #约束条件，权重向量和 =1

    if target_fun_name == 'max_sharpe':
        target_fun = negative_sharpe_ratio
    elif target_fun_name == 'min_std':
        target_fun = positive_std
    elif target_fun_name == 'max_return':
        target_fun = negative_return

    # scopt.minimize格式要求：weights是变量，后面是固定的数据，是额外参数
    results = scopt.minimize(target_fun, weights, (means, sig, risk_free_rate),
                             method='SLSQP',
                             constraints=constraints,
                             bounds=bounds)

    results = scopt.minimize(target_fun, weights, (means, sig, risk_free_rate),
                             method='SLSQP',
                             constraints=constraints,
                             bounds=bounds)

    if not results.success:  # handle error
        raise Exception(results.message)
    mean_sharpe = results.x.dot(means)
    std_sharpe = calc_portfolio_std(sigma=sig, weights=results.x)

    return {'Means': mean_sharpe,
            'Stds': std_sharpe,
            'Weights': results.x}

if __name__ == '__main__':
    np.random.seed(0)
    data = np.random.uniform(-0.5, 0.5, (1000, 7))
    df = pd.DataFrame(data, columns=['材料', '工业', '可选', '消费', '医药', '金融', '信息'])
    returns = df  #列索引为股票名，行索引为时间

    risk_free_rate = 0  #   1.03 ** (1 / 365) - 1
    T_sellshort = optimize_portfolio(returns = returns)
    print('组合收益率为:', T_sellshort['Means'], '\n', '组合标准差为：', T_sellshort['Stds'], '\n', '组合权重为；',
          T_sellshort['Weights'])

    T_sellshort['Weights']

    print(type(T_sellshort['Weights']))
    weights_df = pd.DataFrame(T_sellshort['Weights'], columns=['weight'])

    print(weights_df.head())
