import  pandas as pd
import numpy as np


def evaluate(ret: pd.DataFrame):

    df = ret
    df.reset_index(inplace=True)
    df = df.set_index(['date'])
    # 计算日收益率
    daily_returns = df['daily_returns']

    # 计算年化收益率
    annual_returns = (1 + daily_returns).cumprod()[-1]**(252/len(daily_returns))-1
    # 计算年化波动率
    annual_volatility = np.std(daily_returns) * np.sqrt(252)

    # 计算最大回撤
    cum_returns = (1 + daily_returns).cumprod()
    cum_max_returns = cum_returns.cummax()
    drawdown = (cum_max_returns - cum_returns) / cum_max_returns
    max_drawdown = drawdown.max()

    # 计算起止时间
    end_date = drawdown.idxmax()  # 最大回撤结束时间
    start_date = cum_returns[:end_date].idxmax()  # 最大回撤开始时间


    def calculate_sharpe_ratio(daily_returns):
        '''计算夏普比率'''
        annualized_return = np.mean(daily_returns) * 252  # 年化收益率
        annualized_volatility = np.std(daily_returns) * np.sqrt(252)  # 年化波动率
        sharpe_ratio = (annualized_return -0) / annualized_volatility if annualized_volatility != 0 else 0
        return sharpe_ratio
    # 计算夏普比率
    sharpe_ratio = calculate_sharpe_ratio(daily_returns)


    # 计算盈利和亏损
    profits = daily_returns[daily_returns > 0]
    losses = daily_returns[daily_returns <= 0]

    # 计算胜率和盈亏比
    win_rate = len(profits) / len(daily_returns)

    ret = daily_returns

    report = pd.DataFrame({})
    nav = (ret + 1).cumprod()
    nav = nav.to_frame()
    #print(nav.iloc[0:20])
    #print(nav.iloc[-21, :])

    #print(type(nav))

    report['近1日收益'] = nav.iloc[-1, :] / nav.iloc[-2, :] - 1
    report['近1周收益'] = nav.iloc[-1, :] / nav.iloc[-6, :] - 1
    report['近1月收益'] = nav.iloc[-1, :] / nav.iloc[-21, :] - 1
    report['近3月收益'] = nav.iloc[-1, :] / nav.iloc[-61, :] - 1
    report['近半年收益'] = nav.iloc[-1, :] / nav.iloc[-121, :] - 1
    if len(nav) >= 251:
        report['近1年收益'] = nav.iloc[-1, :] / nav.iloc[-251, :] - 1
    else:
        report['近1年收益'] = np.nan
    report['2023年收益'] = nav.iloc[-1, :] / nav.loc[:'2022-12-31', :].iloc[-1,:] - 1
    report['年化收益'] = annual_returns
    report['年化波动'] = annual_volatility
    report['夏普比率'] = sharpe_ratio
    report['开仓胜率'] = win_rate







    # 计算开仓盈亏比
    positive_returns = ret[ret > 0]
    negative_returns = ret[ret < 0]
    report['开仓盈亏比'] = -positive_returns.mean() / negative_returns.mean()

    # 计算年化开仓次数
    trading_days_per_year = 252
    total_days = len(ret)
    trading_days = np.sum(ret != 0)
    report['年化开仓次数'] = trading_days_per_year * trading_days / total_days

    report['年化盈利次数'] = report['年化开仓次数'] * report['开仓胜率']
    report['最大回撤'] = max_drawdown
    report['Calmar'] = report['年化收益'] / report['最大回撤']
    report['最大回撤起止时间'] = f"{start_date} - {end_date}"

    from datetime import datetime

    if isinstance(start_date, str):
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
    elif isinstance(start_date, pd.Timestamp):
        start_date = start_date.to_pydatetime()
        end_date = end_date.to_pydatetime()

    report['最大回撤持续天数'] = (end_date - start_date).days
    report['统计天数'] = nav.shape[0]

    report = report.reset_index()
    return report