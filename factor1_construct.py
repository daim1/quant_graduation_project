import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from scipy.stats import spearmanr
from scipy import stats as sci
import factor2_data_process as fprocess
import factor3_test as ftest
import concurrent.futures
import time
import warnings

import os
from openpyxl import load_workbook
#from Quant_Che.Tool.tool_function_time import timer
#from Quant_Che.Stocks_Multifactor.b_factor_test_function_day import _factor_dict,_test_factor

"""
构造因子
这里把构造因子，数据处理，多因子集中测试，三个功能集成到了一起，

由于环境版本升级，函数.items()需要改为.items()
"""


def turn_amt_corr(N_params):
    N, turn , amt = N_params
    past_N_factor = pd.DataFrame()    #每个股票的列上，做时间滚动窗口
    for column_name in turn.columns:
        turn_series = turn[column_name]
        amt_series = amt[column_name]
        past_N_factor[column_name] = turn_series.rolling(window=N).corr(amt_series)
    past_N_factor.index.name = f'past_{N}_turn_amt_corr'
    print('因子构造完成:',past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor

def netflow_ratio(N_params):
    N, net = N_params
    def cal_netflow_ratio(roll_net, N):
        if roll_net.shape[0] < N:
            return np.nan
        netflow_ratio_ = roll_net.sum() / roll_net.abs().sum()
        return netflow_ratio_

    past_N_factor = pd.DataFrame()
    for column_name, net_series in net.items():  #每个股票的列上，做时间滚动窗口
        #print(N, f'Column Name: {column_name}')
        past_N_factor[column_name] = net_series.rolling(N).apply(lambda x: cal_netflow_ratio(x, N))
    past_N_factor.index.name = f'past_{N}_netflow_ratio'
    print('因子构造完成:',past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor


def price_change(N_params):
    N, close_price = N_params
    def cal_price_change(roll_pri, N):
        if roll_pri.shape[0] < N:
            return np.nan
        price_change_ = (roll_pri.values[-1] / roll_pri.values[0] - 1)  # 过去2天累计收益率 == 当天收益率，累计收益率因为第一个数字的不同，而导致略有不同
        return price_change_

    past_N_factor = pd.DataFrame()
    for column_name, price_series in close_price.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  price_series.rolling(N).apply(lambda x: cal_price_change(x, N))
    past_N_factor.index.name = f'past_{N}_price_change'
    print('因子构造完成:',past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor


def price_position(N_params):
    N, close_price = N_params
    def cal_price_position(roll_pri, N):
        if roll_pri.shape[0] < N:
            return np.nan
        price_position = (roll_pri.values[-1] - roll_pri.mean()) / (roll_pri.max() - roll_pri.min())  # 价格相对位置
        return price_position

    past_N_factor = pd.DataFrame()
    for column_name, price_series in close_price.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  price_series.rolling(N).apply(lambda x: cal_price_position(x, N))
    past_N_factor.index.name = f'past_{N}_price_position'
    print('因子构造完成:',past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor

def price_slope(N_params):
    N, close_price = N_params
    def cal_slope(roll_pri, N):
        if roll_pri.shape[0] < N:
            return np.nan
        x = np.arange(1, N + 1)
        y = roll_pri.values / roll_pri.values[0]
        lr = LinearRegression().fit(x.reshape(-1, 1), y)
        slope = lr.coef_[0]  # 斜率
        r_squared = lr.score(x.reshape(-1, 1), y)  # 决定系数R2
        slope_score = 10000 * slope * r_squared  # 得分
        return slope_score

    past_N_factor = pd.DataFrame()
    for column_name, price_series in close_price.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  price_series.rolling(N).apply(lambda x: cal_slope(x, N))
    past_N_factor.index.name = f'past_{N}_price_slope'
    print('因子构造完成:',past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df, L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df, L_excess_cum_returns, LS_cum_returns, past_N_factor


def price_spearman(N_params):
    N, close_price = N_params
    def cal_spearman(roll_pri, N):
        if roll_pri.shape[0] < N:
            return np.nan
        x = np.arange(1, N + 1)
        y = roll_pri.values / roll_pri.values[0]
        spearman_rho, _ = spearmanr(x, y)  # 斯皮尔曼秩相关系数
        return spearman_rho

    past_N_factor = pd.DataFrame()
    for column_name, price_series in close_price.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  price_series.rolling(N).apply(lambda x: cal_spearman(x, N))
    past_N_factor.index.name = f'past_{N}_price_spearman'
    print('因子构造完成:',past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor




def cum_ret(N_params):
    N, ret = N_params
    def cal_cmu_ret(roll_ret, N):
        if roll_ret.shape[0] < N:
            return np.nan
        cumulative_return = (roll_ret + 1).prod() - 1
        return cumulative_return

    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name] = ret_series.rolling(N).apply(lambda x: cal_cmu_ret(x,N))
    past_N_factor.index.name   = f'past_{N}_cmu_ret'
    print('因子构造完成:',past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor



def ret_sharpe(N_params):
    N, ret = N_params
    def calculate_sharpe(roll_ret, N,column_name, ret_series):

        if len(roll_ret) < N:
            return np.nan
        x_1 = roll_ret[(roll_ret > np.nanmin(roll_ret)) & (roll_ret < np.nanmax(roll_ret))]  # 去除最大值和最小值
        if x_1.std() == 0:
            print(roll_ret, N, column_name, ret_series)
            print('分母为0报错！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！')
            sharpe = np.nan  # 设置为 NaN
        else:
            sharpe = float(x_1.mean() / x_1.std())
        #print(sharpe)
        return sharpe

    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  ret_series.rolling(N).apply(lambda x: calculate_sharpe(x,N,column_name, ret_series))
    past_N_factor.index.name = f'past_{N}_ret_sharpe'
    print('因子构造完成:',past_N_factor.index.name)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor


def ret_IR(N_params):
    N, ret_excess = N_params
    def calculate_IR(roll_ret, N,column_name, ret_series):
        if len(roll_ret) < N:
            return np.nan
        x_1 = roll_ret[(roll_ret > np.nanmin(roll_ret)) & (roll_ret < np.nanmax(roll_ret))]  # 去除最大值和最小值
        if x_1.std() == 0:
            print(roll_ret, N, column_name, ret_series)
            print('分母为0报错！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！')
            sharpe = np.nan  # 设置为 NaN
        else:
            sharpe = float(x_1.mean() / x_1.std())
        #print(sharpe)
        return sharpe

    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret_excess.items():  #
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  ret_series.rolling(N).apply(lambda x: calculate_IR(x,N,column_name, ret_series))
    past_N_factor.index.name = f'past_{N}_ret_IR'
    print('因子构造完成:', past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor

def ret_path(N_params):
    N, ret = N_params
    def cal_path_ret(roll_ret, N):
        if roll_ret.shape[0] < N:
            return np.nan
        roll_ret_1 = roll_ret[(roll_ret > np.nanmin(roll_ret)) & (roll_ret < np.nanmax(roll_ret))]  # 去除最大值和最小值
        path_ret = roll_ret_1.sum() / roll_ret_1.abs().sum()  # 路径收益
        return path_ret

    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  ret_series.rolling(N).apply(lambda x: cal_path_ret(x, N))
    past_N_factor.index.name = f'past_{N}_ret_path'
    print('因子构造完成:', past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor

def ret_path_risk_adj(N_params):
    N, ret = N_params
    def cal_path_ret_risk_adj(roll_ret, N):
        if roll_ret.shape[0] < N:
            return np.nan
        roll_ret_1 = roll_ret[(roll_ret > np.nanmin(roll_ret)) & (roll_ret < np.nanmax(roll_ret))]  # 去除最大值和最小值
        cum_ret = (1 + roll_ret_1).cumprod() - 1
        path_ret = cum_ret.iloc[-1] / roll_ret_1.abs().sum()  # 路径收益
        return path_ret

    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  ret_series.rolling(N).apply(lambda x: cal_path_ret_risk_adj(x, N))
    past_N_factor.index.name = f'past_{N}_ret_path_risk_adj'
    print('因子构造完成:', past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor

def ret_auto_corr(N_params):
    N, ret = N_params
    def cal_auto_corr(roll_ret, N):
        if roll_ret.shape[0] < N:
            return np.nan
        auto_corr = roll_ret.autocorr()
        return auto_corr

    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  ret_series.rolling(N).apply(lambda x: cal_auto_corr(x, N))
    past_N_factor.index.name = f'past_{N}_ret_auto_corr'
    print('因子构造完成:', past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor

def ret_t_stat(N_params):
    """构造日收益率的t值因子"""
    N, ret = N_params
    def cal_t_stat(roll_ret, N):
        if roll_ret.shape[0] < N:
            return np.nan
        t_stat, _ = sci.ttest_1samp(roll_ret * 10, 0)
        return t_stat
    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  ret_series.rolling(N).apply(lambda x:cal_t_stat(x, N))
    past_N_factor.index.name = f'past_{N}_ret_t_stat'
    print('因子构造完成:', past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor

def ret_p_value(N_params):
    N, ret = N_params
    def cal_p_value(roll_ret, N):
        if roll_ret.shape[0] < N:
            return np.nan
        _, p_value = sci.ttest_1samp(roll_ret * 10, 0)
        return p_value

    past_N_factor = pd.DataFrame()
    for column_name, ret_series in ret.items():
        #print(N,f'Column Name: {column_name}')
        past_N_factor[column_name]      =  ret.rolling(N).apply(lambda x: cal_p_value(x, N))
    past_N_factor.index.name = f'past_{N}_ret_p_value'
    print('因子构造完成:', past_N_factor.index.name)
    past_N_factor = fprocess.process_df(past_N_factor)
    factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns = ftest.factor_statistics_N((past_N_factor,ret))
    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns, past_N_factor






if __name__ == '__main__':
    print("==================================因子构造、数据处理、多因子分析==================================")
    warnings.filterwarnings("ignore")

    t1 = time.time()

    close_price = 1
    '''close_price = pd.read_excel('中信三级行业_日频_因子.xlsx', sheet_name='CLOSE').drop(columns=['CI005488',"CI005510", 'CI005566'])
    close_price.set_index('Date', inplace=True)
    '''

    net = 1
    '''volume = pd.read_excel('中信三级行业_日频_因子.xlsx', sheet_name='VOLUME').drop(columns=['CI005488',"CI005510", 'CI005566'])
    volume.set_index('Date', inplace=True)
    rs = ret.applymap(lambda x: 0 if x == 0 else (x / abs(x) if pd.notnull(x) else 0))
    net = rs * volume
    '''


    ret = pd.read_excel('中信三级行业_日频_因子.xlsx', sheet_name='PCT_CHG').drop(columns=['CI005488', "CI005510", 'CI005566'])
    ret.set_index('Date', inplace=True)
    ret = ret / 100

    '''
    ret = pd.read_excel('中信三级行业_日频_因子.xlsx', sheet_name='PCT_CHG_TODAY').drop(columns=['CI005488',"CI005510", 'CI005566'])
    ret.set_index('Date', inplace=True)


    turn = pd.read_excel('中信三级行业_日频_因子.xlsx', sheet_name='TURN').drop(
        columns=['CI005488', "CI005510", 'CI005566'])
    turn.set_index('Date', inplace=True)

    amt = pd.read_excel('中信三级行业_日频_因子.xlsx', sheet_name='AMT').drop(
        columns=['CI005488', "CI005510", 'CI005566'])
    amt.set_index('Date', inplace=True)
    amt = amt / 100000000
    '''

    '''
    def subtract_mean(row):
        return row - row.mean()
    ret_excess = ret.apply(subtract_mean, axis=1)   #重新命名，不要改变原有值
    '''
    ret_excess = 0


    tss = np.arange(20, 60, 5)
    ret_param_list = []
    for N in tss:
        params_tuple = (N, ret)
        ret_param_list.append(params_tuple)

    """
    price_param_list = []
    for N in tss:
        params_tuple = (N, close_price)
        price_param_list.append(params_tuple)

    net_param_list = []
    for N in tss:
        params_tuple = (N, net)
        net_param_list.append(params_tuple)

    ret_excess_param_list = []
    for N in tss:
        params_tuple = (N, ret_excess)
        ret_excess_param_list.append(params_tuple)

    turn_amt_param_list = []
    for N in tss:
        params_tuple = (N, turn,amt)
        turn_amt_param_list.append(params_tuple)
    """

    pool = concurrent.futures.ProcessPoolExecutor(max_workers=11)
    #dataframes_list = list(pool.map(turn_amt_corr, turn_amt_param_list))   #构造不同因子，只需要修改这里的函数，其他不动
    dataframes_list = list(pool.map(ret_sharpe, ret_param_list))   #构造不同因子，只需要修改这里的函数，其他不动


    first_dfs = [tuple_df[0] for tuple_df in dataframes_list]     #前4个数据框是不同N的因子测试结果
    factor_test_statistics = pd.concat(first_dfs, axis=0)

    second_dfs = [tuple_df[1] for tuple_df in dataframes_list]
    factor_cum_ic = pd.concat(second_dfs, axis=1)

    third_dfs = [tuple_df[2] for tuple_df in dataframes_list]
    factor_LE_cum_ret = pd.concat(third_dfs, axis=1)

    fourth_dfs = [tuple_df[3] for tuple_df in dataframes_list]
    factor_LS_cum_ret= pd.concat(fourth_dfs, axis=1)

    factor_df_list = [tuple_df[4] for tuple_df in dataframes_list]  #第5个返回值，是不同N的因子数据框，组成一个序列

    factor_name = factor_df_list[0].index.name[7:]    #第一个数据框索引名称可能是past_3_price_slope，两者数量不同
    print('\n\n因子文件名：', factor_name)

    with pd.ExcelWriter(f'毕设_{factor_name}_tod_因子测试报表_.xlsx') as writer:
        print('写入因子测试报表ing')
        factor_test_statistics.to_excel(writer, sheet_name='factor_test_statistics')
        factor_cum_ic.to_excel(writer, sheet_name='factor_cum_ic')
        factor_LE_cum_ret.to_excel(writer, sheet_name='factor_LE_cum_ret')
        factor_LS_cum_ret.to_excel(writer, sheet_name='factor_LS_cum_ret')
    print("完成输出：毕设_{factor_name}_tod_因子测试报表_.xlsx")


    with pd.ExcelWriter(f'毕设_{factor_name}_tod.xlsx', engine='openpyxl') as writer:
        for df in factor_df_list:
            print('writing: ' , df.index.name)
            df.to_excel(writer, sheet_name=df.index.name)

    print("完成输出：因子值数据框")

    t2 = time.time()
    print("用时：", t2 - t1)
    print(666)

'''
    with pd.ExcelWriter(f'{factor_name}_abs_tod.xlsx', engine='openpyxl') as writer:
        for df in factor_df_list:
            df.index.name = f'{df.index.name}_abs'
            print('writing: ' , df.index.name)
            df.abs().to_excel(writer, sheet_name=df.index.name)      #时间列是索引，不用管
'''
