import pandas as pd
from Factor0_Analysis_fuc import *
import factor2_data_process as fprocess
import factor3_test as ftest

import factor5_PCA as fpca
import factor6_multi_reg as freg
import Factor0_Markowitz_portfolio as fopt


import concurrent.futures
import time


"""
输入：多个因子数据框，ret数据框
处理：对多个因子，按照不同方式做复合
输出：复合因子测试报表.xlsx
"""

def combine_factors(factor_list, method):  #这个函数用不到
    if method == 'multi':
        combined_factor = pd.DataFrame()
        for df in factor_list:
            if combined_factor.empty:
                combined_factor = df
            else:
                combined_factor *= df
    elif method == 'mean':
        combined_factor = sum(factor_list) / len(factor_list)

    elif method == 'sum':
        combined_factor = sum(factor_list)
    return combined_factor

def rank_df(df):
    def rank_row(row):
        return row.rank(method='min', na_option='keep')

    df = df.apply(rank_row, axis=1)
    return df


def rank_pro_df(df):
    def rank_pro_row(row):
        ranked_row = row.rank(method='min', na_option='keep')
        max_rank = ranked_row.max()
        if pd.isna(max_rank):  # 如果最大排名是 NaN
            return ranked_row * 0  # 返回全是 0 的行
        else:
            return ranked_row / max_rank

    df = df.apply(rank_pro_row, axis=1)
    return df

def factor_opt_fun(ic_df_,ret_cov_label,target_fun_name):
    """
    用优化器，计算使得组合后的IC_IR最大的权重配置
    :param ic_df_:
    :param ret_cov_label:
    :param target_fun_name:
    :return:
    """
    factor_opt_df = pd.DataFrame(index=factor_rank_ic_df.index, columns=factor_df_list[0].columns)
    for index in ic_df_.index:
        ic_df_loc = ic_df_.loc[:index].iloc[:-1, ]
        if len(ic_df_loc) < 100:
            continue
        if ret_cov_label == False:
            ret_cov = None
        else:
            ret_cov = pd.DataFrame([df.loc[index] for df in factor_df_list]).T
        weights = fopt.optimize_portfolio(returns=ic_df_loc, ret_cov=ret_cov, risk_free_rate=0,
                                          target_fun_name=target_fun_name, sellshort=False)['Weights']
        weighted_rows = [df.loc[index] * weight for df, weight in zip(factor_df_list, weights)]
        weighted_rows_df = pd.DataFrame([sum(weighted_rows)], columns=factor_df_list[0].columns)
        factor_opt_df.loc[index] = weighted_rows_df.iloc[0]
        print('因子复合优化器：', index,weights,factor_opt_df.loc[index])

    return factor_opt_df

def factor_opt(factor_df_list):
    factor_opt_dict = {}

    factor_opt_dict['rank_ic_opt_IR'] = factor_opt_fun(ic_df_= factor_rank_ic_df,  ret_cov_label = False,  target_fun_name='max_sharpe')
    factor_opt_dict['rank_ic_opt_IR_cov'] = factor_opt_fun(ic_df_= factor_rank_ic_df,  ret_cov_label = True,  target_fun_name='max_sharpe')
    factor_opt_dict['rank_ic_opt_mean'] = factor_opt_fun(ic_df_= factor_rank_ic_df,  ret_cov_label = False,  target_fun_name='max_return')
    factor_opt_dict['ic_opt_IR'] = factor_opt_fun(ic_df_= factor_ic_df,  ret_cov_label = False,  target_fun_name='max_sharpe')
    factor_opt_dict['ic_opt_IR_cov'] = factor_opt_fun(ic_df_= factor_ic_df,  ret_cov_label = True,  target_fun_name='max_sharpe')

    return factor_opt_dict

def cal_ic_roll(factor, ret, M_list):
    """
    每个因子的具体变换方式
    :param factor:
    :param ret:
    :param M_list:
    :return:
    """
    factor_transf_dict = {}

    df_factor, df_ret, N, factor_name = ftest.format_processing(factor,ret)  #不要与原始因子混淆，也不能改变原始因子
    print(f"合并因子中，计算因子IC和beta等：N={N}", factor_name)
    fa = FactorAnalysis(df_factor, df_ret, [1], 10, asset_weight = 'mean')  # 周期要写出list形式
    result = fa.calc_results(adj_periods=1)

    reg_df = result['reg'].copy()
    beta_df = reg_df['beta']
    beta_mean = beta_df.mean()
    factor_beta_mean = factor * beta_mean
    factor_transf_dict['beta_mean'] = factor_beta_mean

    beta_mean_expand = beta_df.expanding().mean()
    factor_beta_mean_expand = factor.mul(beta_mean_expand, axis=0)
    factor_transf_dict['beta_mean_expand'] = factor_beta_mean_expand

    ic_df = result['ic'].copy()
    ic_mean_df = ic_df.mean()
    ic_mean_expand_df = ic_df.expanding().mean()
    ic_ir_df = ic_df.mean() / ic_df.std(ddof=1)
    ic_mean = ic_mean_df['ic']
    ic_mean_expand = ic_mean_expand_df['ic']

    ic_ir  =  ic_ir_df['ic']
    rank_ic_mean = ic_mean_df['rank_ic']
    rank_ic_ir  =  ic_ir_df['rank_ic']
    rank_ic_mean_expand = ic_mean_expand_df['rank_ic']

    factor_ic_mean = factor * ic_mean   #一定是对原始因子，乘以一个权重
    factor_ic_mean_expand = factor.mul(ic_mean_expand, axis=0)
    factor_ic_ir = factor * ic_ir
    factor_rank_ic_mean = factor  * rank_ic_mean
    factor_rank_ic_mean_expand = factor.mul(rank_ic_mean_expand, axis=0)

    factor_rank_ic_ir  = factor  * rank_ic_ir
    factor_transf_dict['ic_mean'] = factor_ic_mean
    factor_transf_dict['ic_mean_expand'] = factor_ic_mean_expand
    factor_transf_dict['ic_ir']   = factor_ic_ir
    factor_transf_dict['rank_ic_mean'] = factor_rank_ic_mean
    factor_transf_dict['rank_ic_mean_expand'] = factor_rank_ic_mean_expand
    factor_transf_dict['rank_ic_ir'] = factor_rank_ic_ir

    for M in M_list:
        print(f"合并因子中，计算因子IC和beta等：滚动窗口M={M}", factor_name)
        ic_mean_rolling = ic_df.rolling(window=M).mean()
        ic_std_rolling = ic_df.rolling(window=M).std(ddof=1)
        ic_ir_rolling = ic_mean_rolling / ic_std_rolling
        # print(ic_mean_rolling,ic_ir_rolling)

        beta_mean_roll = beta_df.rolling(window=M).mean()

        ic_mean_roll = ic_mean_rolling['ic']
        ic_ir_roll = ic_ir_rolling['ic']

        rank_ic_mean_roll = ic_mean_rolling['rank_ic']
        rank_ic_ir_roll = ic_ir_rolling['rank_ic']

        factor_beta_mean_roll = factor.mul(beta_mean_roll, axis=0)
        factor_ic_mean_roll = factor.mul(ic_mean_roll, axis=0)  # 用最原始的因子值，去乘以每期的权重，每一行去乘这一期的权重
        factor_ic_ir_roll = factor.mul(ic_ir_roll, axis=0)
        factor_rank_ic_mean_roll = factor.mul(rank_ic_mean_roll, axis=0)
        factor_rank_ic_ir_roll = factor.mul(rank_ic_ir_roll, axis=0)

        factor_transf_dict[f'beta_mean_roll_{M}'] = factor_beta_mean_roll
        factor_transf_dict[f'ic_mean_roll_{M}'] = factor_ic_mean_roll
        factor_transf_dict[f'ic_ir_roll_{M}'] = factor_ic_ir_roll
        factor_transf_dict[f'rank_ic_mean_roll_{M}'] = factor_rank_ic_mean_roll
        factor_transf_dict[f'rank_ic_ir_roll_{M}'] = factor_rank_ic_ir_roll

    return factor_transf_dict, ic_df['ic'], ic_df['rank_ic']


def factor_transform(factor_df_list, ret, M_list):
    """
    遍历单个因子，计算单个因子在每期的权重，权重与自身相乘，得到变换后因子
    每个因子对应一个字典
    :param factor_df_list: 带索引的因子数据框序列
    :param ret:
    :param M_list:
    :return:
    """
    factor_dict_list = []  # 先转换为新的因子值，比如行内的秩，再对不同因子做合并，每个因子一个字典
    factor_ic_list = []
    factor_rank_ic_list = []
    for df in factor_df_list:
        factor_dict = {}
        r_df = rank_df(df)
        rp_df = rank_pro_df(df)
        factor_dict['factor_sum'] = df
        factor_dict['rank_sum'] = r_df
        factor_dict['rank_proportion_mul'] = rp_df

        factor_transf_dict, ic_df, ran_ic_df = cal_ic_roll(df, ret, M_list)
        factor_dict.update(factor_transf_dict)
        factor_dict_list.append(factor_dict)
        factor_ic_list.append(ic_df)
        factor_rank_ic_list.append(ran_ic_df)

    return factor_dict_list,factor_ic_list,factor_rank_ic_list


def transformed_factor_sum(factor_dict_list):
    """
    将变换后的多个因子，按照相同变换类型(key)进行相加
    :param factor_dict_list:
    :return:
    """
    from collections import defaultdict
    combined_dict = defaultdict(pd.DataFrame)      # 初始化一个 defaultdict，它可以自动生成默认值
    for factor_dict in factor_dict_list:
        for key, df in factor_dict.items():
            if combined_dict[key].empty:
                combined_dict[key] = df  # 如果结果字典中的对应键值还是空的话，就直接赋值
            else:
                if key == 'rank_proportion':
                    combined_dict[key] *= df
                else:
                    combined_dict[key] += df  # 否则，就进行相加
    combined_dict = dict(combined_dict)      # 这个字典，是包含所有合并之后的因子数据框，最后需要把 defaultdict 转回普通的 dict
    return combined_dict


'''
factor_classification = 'ret_auto_corr'  #因子名称是原始的
xlsx = pd.ExcelFile(f'E:\高盈国际--实习\Gitlab_Quant_code\quant_wu\Multifactor_model\\{factor_classification}_tod.xlsx')
#factor_1 = xlsx.parse(f'past_10_{factor_classification}')
factor_2 = xlsx.parse(f'past_17_{factor_classification}')
factor_3 = xlsx.parse(f'past_19_{factor_classification}')
factor_df_list = [factor_2, factor_3]  #改变以上几个参数即可，其他不用变
'''

if __name__ == '__main__':
    print("==================================因子复合==================================")

    t1 = time.time()

    """提供带索引的因子数据框list, 滚动加权的窗口数量参数"""
    filename_out = 'final_factor'
    M_list = np.arange(120, 211, 30)  # 这几个参数，用与提取因子历史截面滚动IC平均值，这是不同因子的权重是每个调仓日都会调整 (120, 211, 15)
    M_reg_list =  np.arange(50, 241, 50)  # (50, 241, 10)
    factor1 = pd.read_excel('ret_auto_corr_tod_combined.xlsx', sheet_name='com_fac_mean')
    factor2 = pd.read_excel('price_position_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor3 = pd.read_excel('ret_t_stat_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor_df_list = [factor1, factor2, factor3]  # 改变以上几个参数即可，其他不用变
    for df in factor_df_list:
        df.set_index(df.columns[0], inplace=True)  # 设置时间列为索引

    """"以下只需调整导出的文件名，可以不改，其他不动"""
    ret = pd.read_excel('中信三级行业_日频_因子.xlsx', sheet_name='PCT_CHG').drop(columns=['CI005488', "CI005510", 'CI005566'])
    ret.set_index(ret.columns[0], inplace=True)
    ret = ret / 100

    """"
    遍历单个因子，计算单个因子在每期的权重，权重与自身相乘，得到变换后因子;
    将变换后的多个因子，按照相同变换类型(key)进行相加;
    """
    factor_dict_list, factor_ic_list, factor_rank_ic_list = factor_transform(factor_df_list, ret, M_list)
    combined_dict = transformed_factor_sum(factor_dict_list)


    """"加入其他合并方式的因子，如因子复合优化器、多元回归、PCA分解"""
    factor_ic_df = pd.concat(factor_ic_list, axis=1)
    factor_rank_ic_df = pd.concat(factor_rank_ic_list, axis=1)
    factor_opt_dict = factor_opt(factor_df_list)

    ret_multi_reg_dict = freg.ret_multi_reg_fuc(factor_df_list, ret, M_reg_list)  # 把原有因子list，用多元回归加权合并，因为加权回看周期的不同，有多个合并因子
    factor_pca_dict  =  fpca.PCA_construct(factor_df_list)  # 把原有因子list，用pca进行分解，因子数量不变，传入参数是带时间索引的数据框list
    combined_dict = {**combined_dict, **factor_opt_dict, **ret_multi_reg_dict, **factor_pca_dict}  # 可以把pca分解后的因子list，再用多元回归合并，但是会和前面的键值重复，所以要单独测试
    print('完成因子合并！')

    """以下代码不用动，先写入合并后的因子"""
    with pd.ExcelWriter(f'毕设_{filename_out}.xlsx') as writer:
        for key, df in combined_dict.items():
            df_1 = fprocess.standard_df(df).copy()   #对合并后的因子值，进行标准化
            df_1.index.name = key
            combined_dict[key]  = df_1    #合并后的因子值，索引仍然为因子名称
            df_1.to_excel(writer, sheet_name=key)
            print('写入Excel：', df_1.index.name)
    print('合并因子写入Excel完成! ')

    """"对合并后的所有因子，进行集中测试"""
    combined_factor_list = []
    for df in combined_dict.values():
        df_tuple = (df, ret)  # 创建一个元组
        combined_factor_list.append(df_tuple)  # 将元组添加到列表中

    pool = concurrent.futures.ProcessPoolExecutor(max_workers=11)
    dataframes_list = list(pool.map(ftest.factor_statistics_N, combined_factor_list))  # 需要返回为dataframes格式
    first_dfs = [tuple_df[0] for tuple_df in dataframes_list]
    factor_test_statistics = pd.concat(first_dfs, axis=0)
    second_dfs = [tuple_df[1] for tuple_df in dataframes_list]
    factor_cum_ic = pd.concat(second_dfs, axis=1)
    third_dfs = [tuple_df[2] for tuple_df in dataframes_list]
    factor_LE_cum_ret = pd.concat(third_dfs, axis=1)
    fourth_dfs = [tuple_df[3] for tuple_df in dataframes_list]
    factor_LS_cum_ret = pd.concat(fourth_dfs, axis=1)

    with pd.ExcelWriter(f'毕设_{filename_out}_因子测试报表_.xlsx') as writer:
        factor_test_statistics.to_excel(writer, sheet_name='factor_test_statistics')
        factor_cum_ic.to_excel(writer, sheet_name='factor_cum_ic')
        factor_LE_cum_ret.to_excel(writer, sheet_name='factor_LE_cum_ret')
        factor_LS_cum_ret.to_excel(writer, sheet_name='factor_LS_cum_ret')

    t2 = time.time()
    print("用时：", t2 - t1)
    print('因子复合完成 !')


