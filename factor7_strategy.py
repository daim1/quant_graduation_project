import pandas as pd
import numpy as np
from Factor0_Analysis_fuc import*
import factor3_test as ftest
import concurrent.futures
import time
from Factor0_Markowitz_portfolio import optimize_portfolio
from Tool.tool_evaluate_realtime import evaluate

def factor_strategy(params_tuple):
    final_factor, ret, n_group, target_num, list_periods, weight_method,K = params_tuple

    df_factor, df_ret, N, factor_name = ftest.format_processing(final_factor, ret)
    fa = FactorAnalysis(df_factor, df_ret, list_periods , n_group, asset_weight='mean')  # 周期要写出list形式
    result_dict_list = fa.calc_multiple_periods()
    result_dict  = result_dict_list[0]

    factor_period = result_dict['factor_period_df'].T
    ret_period = result_dict['ret_period_df'].T  #时间会缩短一些，这是调仓日期节点的周期内累计ret
    group_period = result_dict['group_period_df'].T

    trade_ret_df = pd.DataFrame(columns=group_period.index)  #每列为调仓日期，值为对应的收益率

    """
    调仓日list，计算权重必须使用调仓日前一天及之前的数据，调仓日是周期的第一天;
    第一个调仓日可以用，因为已经有调仓日前一天的因子值，以及排名;
    在mvo组合优化中，需要有较多窗口的历史收益率来计算；
    """

    for index in group_period.index:
        group_row = group_period.loc[index]  #上期因子值得到的排名， 变成带着列名code为索引的 Series
        target_code = group_row[group_row == n_group - target_num +1 ].index.tolist()
        today_ret = ret_period.loc[index, target_code]  # 要使用该周期内累计收益率

        if weight_method == 'mean':
            trade_ret_df[index] = [today_ret.mean()]  #对一列要传入list才行，不能单单传入一个值
        elif weight_method == 'score':
            today_factor = factor_period.loc[index, target_code] #上期的因子值,factor_period是调仓日前一天的因子，因为之前下移了一次
            weights = today_factor / today_factor.sum()
            trade_ret_df[index] = [(today_ret * weights).sum()]
        else:
            sub_ret_df = ret.loc[:index, target_code]
            sub_ret_df = sub_ret_df.iloc[:-1, ].iloc[-K:]  # 截止调仓日前一天的原始每日ret，用于马科维兹模型训练
            weights = optimize_portfolio(returns = sub_ret_df, risk_free_rate=0, target_fun_name = weight_method, sellshort=False)['Weights']
            weights_df = pd.DataFrame(weights).T
            weights_df.columns = target_code
            weight_ret = weights_df * today_ret
            trade_ret_df[index] = weight_ret.T.sum()

    trade_ret_df = trade_ret_df.T
    trade_ret_df.columns  = ['daily_returns']

    #调仓周期不为1时，将间隔的那些时间填充为0，再做回测
    ret_start = ret.loc[group_period.index[0]:]
    new_index = ret_start.index
    trade_ret_df = trade_ret_df.reindex(new_index, fill_value=0)
    print(trade_ret_df)

    cum_ret_df = pd.DataFrame()
    cum_ret_df[f'@{weight_method}@{n_group}@{list_periods}@{target_num}']\
        = (1 + trade_ret_df['daily_returns']).cumprod() - 1
    cum_ret_df.index.name  = final_factor.index.name

    params_dict = {
        '策略名称':       final_factor.index.name,
        '资产配置': weight_method,
        '分层数量':       n_group,
        '调仓周期':  list_periods,
        '目标组': target_num,
    }

    params_df = pd.DataFrame(params_dict)

    trade_ret_df.index.name = 'date'
    trade_ret_df = trade_ret_df.reset_index()  # 把索引改为一列
    evaluate_report = evaluate(trade_ret_df)
    evaluate_param_df = pd.concat([params_df, evaluate_report], axis=1)
    print(evaluate_param_df)

    return  evaluate_param_df, cum_ret_df

if __name__ == '__main__':
    print("==================================构建策略==================================")

    t1 = time.time()

    """提供测试因子需要的ret"""
    ret = pd.read_excel('中信三级行业_日频_因子.xlsx',
                        sheet_name='PCT_CHG').drop(columns=['CI005488', "CI005510", 'CI005566'])
    ret.set_index(ret.columns[0], inplace=True)
    ret = ret / 100

    """"提供测试需要的因子list, 参数元组"""
    filename_out = 'final_factor_4'  # 大类因子测试只用改变这里和文件名就行就行
    final_factor = pd.read_excel('final_factor.xlsx', sheet_name='ic_ir_roll_150')
    final_factor.set_index(final_factor.columns[0], inplace=True)  # 设置时间列为索引


    weight_method = 'max_sharpe'  #'score'  'mean', 'max_sharpe' , 'min_std',  'max_return'
    n_group = 10
    target_num = 1  # 因子最大的第 target_num 组
    list_periods = [1]
    K = 10000  # 优化器使用的窗口数量

    '''
    params_tuple  = (final_factor, ret, n_group, target_num, list_periods, weight_method)
    cum_ret_df , evaluate_param_df = final_factor(params_tuple)'''
    param_tuple_list = []
    for weight_method in ['min_std',  'max_return' ]:
        for n_group in [5,10,20,40]:
            for target_num in  range(1, n_group):
                for list_periods in [ [1],]: # [ [1],[3],[5],[7],[10],[15],[20], [30],[60]]:
                    params_tuple = (final_factor, ret, n_group, target_num, list_periods, weight_method,K)
                    param_tuple_list.append(params_tuple)

    pool = concurrent.futures.ProcessPoolExecutor(max_workers=11)
    dataframes_list = list(pool.map(factor_strategy, param_tuple_list)) #需要返回为dataframes格式
    first_dfs = [tuple_df[0] for tuple_df in dataframes_list]
    strategy_statistics = pd.concat(first_dfs, axis=0)
    second_dfs = [tuple_df[1] for tuple_df in dataframes_list]
    cum_ret_df = pd.concat(second_dfs, axis=1)

    with pd.ExcelWriter(f'毕设_{filename_out}_多因子策略回测报表.xlsx') as writer:
        strategy_statistics.to_excel(writer, sheet_name='Backtest Statistics')
        cum_ret_df.to_excel(writer, sheet_name='Backtest Cumulative Returns')

    t2 = time.time()
    print("用时：", t2 - t1)
    print('输出完成：毕设_{filename_out}_多因子策略回测报表.xlsx')

