import pandas as pd
from sklearn.linear_model import LinearRegression
import factor2_data_process as fprocess
import factor3_test as ftest
from Factor0_Analysis_fuc import *
from functools import reduce
import concurrent.futures
import time

def rank_df(df):
    def rank_row(row):
        return row.rank(method='min', na_option='keep')

    df = df.apply(rank_row, axis=1)
    return df

def multi_reg( factor_df_list ,ret):
    """
    计算多元回归的回归系数
    :param factor_df_list:
    :param ret:
    :return:
    """
    beta_df = pd.DataFrame(index=ret.index, columns=['beta' + str(i + 1) for i in range(len(factor_df_list))])
    intercept_df = pd.DataFrame(index=ret.index, columns=['intercept'])
    for idx in ret.index: #每一行，产生一组beta的最小二乘估计，也就是因子收益率
        X_df = pd.concat([f.loc[idx] for f in factor_df_list], axis=1)
        if X_df.isnull().values.any():     # 如果 X 矩阵中包含 NaN，跳过这个 idx
            print('多元回归中，有空行，跳过：',idx)
            continue
        y = ret.loc[idx]
        X = X_df.values
        model = LinearRegression(fit_intercept=True)    # 进行线性回归
        model.fit(X, y)
        for i in range(len(factor_df_list)):      # 保存 beta 参数
            beta_df.loc[idx, 'beta' + str(i + 1)] = model.coef_[i]
        intercept_df.loc[ idx,'intercept'] = model.intercept_
    return beta_df,intercept_df

def ret_multi_reg_fuc(factor_df_list,ret,M_reg_list):
    """
    用昨日因子，拟合今日收益率，得到回归系数beta
    用今日因子，乘以回归系数，再相加，得到合并后的今日因子
    :param factor_df_list: 传入带索引的因子数据框list,以及今日收益率
    :param ret:
    :param M_reg_list:
    :return:
    """
    factor_shift_list = [df.shift(1) for df in factor_df_list]  #先下移一行，用昨日因子，拟合今日收益率
    beta_df, intercept_df = multi_reg(factor_shift_list, ret)
    print(beta_df)

    ret_multi_reg_dict = {}

    beta_mean_series = beta_df.mean()
    print(beta_mean_series)
    fac_beta_mean_list = [factor.multiply(beta_mean_series.iloc[i], axis=0) for i, factor in  enumerate(factor_df_list)]
    fac_beta_mean_df = reduce(lambda a, b: a + b, fac_beta_mean_list).copy()
    ret_multi_reg_dict[f'multi_reg_beta_mean'] = fac_beta_mean_df
    print(f"合并因子中，计算回归系数：", f'multi_reg_beta_mean')

    mean_expanding = beta_df.expanding().mean()
    fac_mean_expanding_list = [factor.multiply(mean_expanding.iloc[:, i], axis=0) for i, factor in  enumerate(factor_df_list)]
    fac_mean_expand_df = reduce(lambda a, b: a + b, fac_mean_expanding_list).copy()
    ret_multi_reg_dict[f'multi_reg_beta_mean_expand'] = fac_mean_expand_df
    print(f"合并因子中，计算回归系数：滚动窗口M_reg=", f'multi_reg_beta_mean_expand')

    for M_reg in  M_reg_list:
        beta_rolling_mean_df = beta_df.rolling(window=M_reg).mean()
        ret_multi_reg_list = [factor.multiply(beta_rolling_mean_df.iloc[:, i], axis=0) for i, factor in  enumerate(factor_df_list)]
        ret_multi_reg_df = reduce(lambda a, b: a + b, ret_multi_reg_list).copy()
        ret_multi_reg_dict[f'multi_reg_beta_rolling_{M_reg}']  = ret_multi_reg_df
        print(f"合并因子中，计算回归系数：滚动窗口M_reg={M_reg}", f'multi_reg_beta_rolling_{M_reg}')

    return ret_multi_reg_dict

"""
#法1：因子不动，beta下移，滚动均值乘以因子，然后不动
#法2：因子下移，昨日因子拟合今日收益率，beta不动，滚动均值乘以原始的当天因子，得明日预期收益率，作为今日及之前数据构造得当日因子
#本质上，是给当期的因子，乘以一个系数
"""


if __name__ == '__main__':

    '''
    xlsx = pd.ExcelFile(f'E:\高盈国际--实习\Gitlab_Quant_code\quant_wu\Multifactor_model\\factor_pca.xlsx')
    sheet_names = xlsx.sheet_names
    factor_df_list = []  # 传入参数为(df, ret) 元组形式
    for sheet_name in xlsx.sheet_names:
        df = xlsx.parse(sheet_name)
        df.set_index(df.columns[0], inplace=True)
        factor_df_list.append(df)
    '''

    """提供带索引的因子数据框list,单独测试pca分解后再用multi_reg合并后的因子"""
    file_name_out = 'factor_multi_reg_ret_rank'
    M_reg_list = np.arange(1, 301, 10)
    factor1 = pd.read_excel('ret_auto_corr_tod_combined.xlsx', sheet_name='com_fac_mean')
    factor2 = pd.read_excel('price_position_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor3 = pd.read_excel('ret_t_stat_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor_df_list = [factor1, factor2, factor3]  # 改变以上几个参数即可，其他不用变
    for df in factor_df_list:
        df.set_index(df.columns[0], inplace=True)  # 设置时间列为索引

    """提供拟合目标，ret，或者ret_rank,要区分拟合目标ret_rank和回测ret"""
    ret = pd.read_excel('E:\高盈国际--实习\Gitlab_Quant_code\quant_wu\Multifactor_model\中信三级行业_日频_因子.xlsx',
                        sheet_name='PCT_CHG').drop(columns=['CI005488', "CI005510", 'CI005566'])
    ret.set_index(ret.columns[0], inplace=True)
    ret = ret / 100
    ret_rank = fprocess.standard_df(rank_df(ret))

    """以下代码不用动，先写入合并后的因子"""
    t1 = time.time()
    ret_multi_reg_dict = ret_multi_reg_fuc(factor_df_list,ret_rank,M_reg_list)
    print('完成因子合并！')
    with pd.ExcelWriter(f'E:\高盈国际--实习\Gitlab_Quant_code\quant_wu\Multifactor_model\\{file_name_out}.xlsx') as writer:
        for key, df in ret_multi_reg_dict.items():
            df_1 = fprocess.standard_df(df).copy()   #对合并后的因子值，进行标准化
            df_1.index.name = key
            ret_multi_reg_dict[key]  = df_1    #合并后的因子值，索引仍然为因子名称
            df_1.to_excel(writer, sheet_name=key)
            print('写入Excel：', df_1.index.name)
    print('合并因子写入Excel完成! ')

    """"对合并后的所有因子，进行集中测试"""
    combined_factor_list = []  #多进程需要的参数元组 list
    for df in ret_multi_reg_dict.values():
        df_tuple = (df, ret)  # 创建一个元组
        combined_factor_list.append(df_tuple)  # 将元组添加到列表中

    pool = concurrent.futures.ProcessPoolExecutor(max_workers=11)
    dataframes_list = list(pool.map(ftest.factor_statistics_N, combined_factor_list))  # 需要返回为dataframes格式
    first_dfs = [tuple_df[0] for tuple_df in dataframes_list]
    factor_test_statistics = pd.concat(first_dfs, axis=0)
    second_dfs = [tuple_df[1] for tuple_df in dataframes_list]
    factor_cum_ic = pd.concat(second_dfs, axis=1)
    third_dfs = [tuple_df[2] for tuple_df in dataframes_list]
    factor_LE_cum_ret = pd.concat(third_dfs, axis=1)
    fourth_dfs = [tuple_df[3] for tuple_df in dataframes_list]
    factor_LS_cum_ret = pd.concat(fourth_dfs, axis=1)

    with pd.ExcelWriter(f'{file_name_out}因子测试报表_.xlsx') as writer:
        factor_test_statistics.to_excel(writer, sheet_name='factor_test_statistics')
        factor_cum_ic.to_excel(writer, sheet_name='factor_cum_ic')
        factor_LE_cum_ret.to_excel(writer, sheet_name='factor_LE_cum_ret')
        factor_LS_cum_ret.to_excel(writer, sheet_name='factor_LS_cum_ret')

    t2 = time.time()
    print("用时：", t2 - t1)
    print('合并因子测试完成 !')

