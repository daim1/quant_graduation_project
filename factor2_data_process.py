import pandas as pd

def standard_df(df):
    def standard_row(row):         # 标准化
        std = row.std()
        if std == 0:
            row[:] = 0  #这样不会影响与其他因子加权
        else:
            row = (row - row.mean()) / std     #zh
        return row

    return df.apply(standard_row, axis=1)


def process_df(df):
    def process_row(row):
        median = row.median()          # 中位数去极值
        mad = abs(row - median).median()
        upper_bound = median + 5 * mad
        lower_bound = median - 5 * mad
        row = row.where(row <= upper_bound, upper_bound)
        row = row.where(row >= lower_bound, lower_bound)
        std = row.std()         # 标准化
        if std == 0:
            row[:] = 0  #这样不会影响与其他因子加权
        else:
            row = (row - row.mean()) / std     #zh
        return row
    df = df.apply(process_row, axis=1)
    print("因子标准化完成：",df.index.name)
    return df








def file_muti_pro(file_name):
    factor_processed = {}

    xlsx = pd.read_excel(f'E:\高盈国际--实习\Gitlab_Quant_code\quant_wu\Multifactor_model\\{file_name}.xlsx', sheet_name=None)

    for factor_name, df in xlsx.items():
        print(f"Processing DataFrame: {factor_name}")
        df.set_index(df.columns[0], inplace=True)
        factor_processed_df = process_df(df)
        factor_processed[factor_name] = factor_processed_df

    with pd.ExcelWriter(f'{file_name}_done.xlsx') as writer:
        for key, df in factor_processed.items():
            print('Writing:',key)
            df.to_excel(writer, sheet_name=key)

#tss = ['ret_sharpe_abs','price_change', 'price_position' , 'price_slope','price_spearman' , 'cmu_ret',  'ret_path', 'ret_auto_corr','ret_t_stat' ,'netflow_ratio','netflow_ratio_abs']

tss = [ 'ret_path','ret_path_abs','netflow_ratio' ,'netflow_ratio_abs']

import concurrent.futures
import time
t1 =  time.time()



if __name__ == '__main__':
    pool = concurrent.futures.ProcessPoolExecutor(max_workers=11)

    dataframes_list = list(pool.map(file_muti_pro, tss)) #需要返回为dataframes格式

    t2 = time.time()
    print("用时：", t2 - t1)

    print(666)

