import pandas as pd
import numpy as np
from Factor0_Analysis_fuc import*
import concurrent.futures
import time


def format_processing(factor,ret):  #输入带时间索引,索引名称为因子名称的原因子数据框，带时间索引的原收益率数据框
    empty_rows = factor.iloc[:, 1:].isna().all(axis=1).sum()  # 计算因子的前面的空行行数
    N = empty_rows + 1
    factor_name = factor.index.name
    df_factor_ = factor.iloc[N - 1:]  # 去掉前面的空行，重新命名，否则会改变上面的变量
    df_ret_ = ret.iloc[N - 1:]  # 改变N后，要重新顺序运行代码，读取数据，否则会用原来的变量
    # print(df_ret_,df_factor_)
    df_ret = df_ret_.iloc[1:-1, :]  # 去掉第一行和最后一行
    df_factor = df_factor_.shift(1).iloc[1:-1, :]  # 下移一行，索引不动，用昨天的因子预测今天的收益率   ,只在因子测试内部下移，其他时候不需要下移
    df_ret.index = pd.to_datetime(df_ret.index)
    df_factor.index = pd.to_datetime(df_factor.index)
    print(f"因子开始测试：N={N}", factor_name)
    return df_factor,df_ret,N,factor_name

def factor_statistics_N(param_tuple):  #此时(def,ret)已经是一个带时间索引的数据框,索引名称为因子名
    df, ret = param_tuple
    df_factor, df_ret, N, factor_name = format_processing(df,ret)

    fa = FactorAnalysis(df_factor, df_ret, [1], 10,asset_weight = 'mean')  #周期要写出list形式

    cum_ic_df = pd.DataFrame()
    L_excess_cum_returns = pd.DataFrame()
    LS_cum_returns  = pd.DataFrame()

    fac_statistics,cum_ic_df[f'{factor_name}'],L_excess_cum_returns[f'{factor_name}'],LS_cum_returns[f'{factor_name}'] = fa.statistics_result_df(kind = 'ic')
    factor_name_df = pd.DataFrame({'factor_name': [factor_name]})
    factor_statistics_df = pd.concat([factor_name_df,fac_statistics], axis=1)
    print('因子测试完成：',factor_statistics_df)

    return factor_statistics_df, cum_ic_df,  L_excess_cum_returns, LS_cum_returns





if __name__ == '__main__':
    t1 = time.time()

    """提供测试因子需要的ret"""
    ret = pd.read_excel('中信三级行业_日频_因子.xlsx',
                        sheet_name='PCT_CHG').drop(columns=['CI005488', "CI005510", 'CI005566'])
    ret.set_index(ret.columns[0], inplace=True)
    ret = ret / 100

    """"提供测试需要的因子list, 参数元组"""
    filename_out = 'factor_pca'  # 大类因子测试只用改变这里和文件名就行就行
    xlsx = pd.ExcelFile(f'{filename_out}.xlsx')
    sheet_names = xlsx.sheet_names
    param_list = []  #传入参数为(df, ret) 元组形式
    for sheet_name in xlsx.sheet_names:
        df = xlsx.parse(sheet_name)
        df.set_index(df.columns[0], inplace=True)
        df_tuple = (df, ret)  # 创建一个元组
        param_list.append(df_tuple)  # 将元组添加到列表中
    #param_list = [param_list[1]]

    """多进程实现因子集中回测"""
    pool = concurrent.futures.ProcessPoolExecutor(max_workers=11)
    dataframes_list = list(pool.map(factor_statistics_N, param_list)) #需要返回为dataframes格式

    first_dfs = [tuple_df[0] for tuple_df in dataframes_list]
    factor_test_statistics = pd.concat(first_dfs, axis=0)
    second_dfs = [tuple_df[1] for tuple_df in dataframes_list]
    factor_cum_ic = pd.concat(second_dfs, axis=1)
    third_dfs = [tuple_df[2] for tuple_df in dataframes_list]
    factor_LE_cum_ret = pd.concat(third_dfs, axis=1)
    fourth_dfs = [tuple_df[3] for tuple_df in dataframes_list]
    factor_LS_cum_ret= pd.concat(fourth_dfs, axis=1)

    with pd.ExcelWriter(f'毕设_{filename_out}_因子测试报表.xlsx') as writer:
        factor_test_statistics.to_excel(writer, sheet_name='factor_test_statistics')
        factor_cum_ic.to_excel(writer, sheet_name='factor_cum_ic')
        factor_LE_cum_ret.to_excel(writer, sheet_name='factor_LE_cum_ret')
        factor_LS_cum_ret.to_excel(writer, sheet_name='factor_LS_cum_ret')

    t2 = time.time()
    print("用时：", t2 - t1)
    print('完成因子集中测试！')

