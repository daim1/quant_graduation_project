import pandas as pd
import numpy as np
from Factor0_Analysis_fuc import *
import factor2_data_process as fprocess
import factor3_test as ftest
from sklearn.decomposition import PCA
import pandas as pd

""""对每期不同因子间的截面数据，做主成分分析，只取第一个"""
def PCA_construct(factor_list):
    pca = PCA(n_components=len(factor_list))  # 初始化 PCA 对象，只保留第一个主成分
    factor_pca_dict = {}
    for i in factor_list[0].index:  # 对每一行进行主成分分析
        rows = [df.loc[i] for df in factor_list]
        df_row = pd.DataFrame(rows).T  # 将 rows 转换为 DataFrame，以便我们可以在其上使用 PCA
        if any(row.isnull().all() for row in rows):
            for k in range(len(factor_list)):
                empty_row = pd.DataFrame(index=[pd.DataFrame(rows).index[0]], columns=pd.DataFrame(rows).columns)
                if i == factor_list[0].index[0]:
                    factor_pca_dict[f'PCA_NO_{k + 1}'] = empty_row
                else:
                    factor_pca_dict[f'PCA_NO_{k + 1}'] = pd.concat([factor_pca_dict[f'PCA_NO_{k + 1}'], empty_row], axis=0)
            continue
        pca_result = pca.fit_transform(df_row)
        pca_df = pd.DataFrame(pca_result, index=df_row.index, columns=[f'{i}' for k in range(len(factor_list))])
        for k in range(len(factor_list)):
            factor_pca_dict[f'PCA_NO_{k + 1}'] = pd.concat(
                [factor_pca_dict[f'PCA_NO_{k + 1}'], pd.DataFrame(pca_df.iloc[:, k]).T], axis=0)
        print(f'第{i}期主成分解释度:', pca.explained_variance_ratio_)
    return factor_pca_dict


if __name__ == '__main__':

    """提供带索引的因子数据框list，文件命名"""
    file_name_out = 'factor_PCA'
    factor1 =  pd.read_excel('ret_auto_corr_tod_combined.xlsx', sheet_name='com_fac_mean')
    factor2 =  pd.read_excel('price_position_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor3 =  pd.read_excel('price_change_done.xlsx', sheet_name='past_5_price_change')
    factor4 =  pd.read_excel('ret_t_stat_combined.xlsx', sheet_name='com_fac_rank_mean')
    factor5 =  pd.read_excel('price_slope_combined.xlsx', sheet_name='com_fac_mean')
    dfs = [factor1, factor2, factor3, factor4,factor5]
    df_names = ['ret_auto_corr', 'price_position', 'price_change','ret_t_stat','price_slope']
    for df in dfs:
        df.set_index(df.columns[0], inplace=True)   #设置时间列为索引

    """提供回测主成分因子需要的ret"""
    ret = pd.read_excel('E:\高盈国际--实习\Gitlab_Quant_code\quant_wu\Multifactor_model\中信三级行业_日频_因子.xlsx',
                        sheet_name='PCT_CHG').drop(columns=['CI005488', "CI005510", 'CI005566'])
    ret.set_index(ret.columns[0], inplace=True)
    ret = ret / 100

    """以下代码不用动，先写入合并后的因子"""
    combined_dict = PCA_construct(dfs)   #主成分分析，返回主成分因子
    with pd.ExcelWriter(f'E:\高盈国际--实习\Gitlab_Quant_code\quant_wu\Multifactor_model\\{file_name_out}.xlsx') as writer:
        for key, df in combined_dict.items():
            df_1 = fprocess.standard_df(df).copy()   #对合并后的因子值，进行标准化
            df_1.index.name = key
            combined_dict[key]  = df_1    #合并后的因子值，索引仍然为因子名称
            df_1.to_excel(writer, sheet_name=key)
            print('写入Excel：', df_1.index.name)
    print('合并因子写入Excel完成! ')


    """"对合并后的所有因子，进行集中测试"""
    combined_factor_list = []
    for df in combined_dict.values():
        df_tuple = (df, ret)  # 创建一个元组
        combined_factor_list.append(df_tuple)  # 将元组添加到列表中

    pool = concurrent.futures.ProcessPoolExecutor(max_workers=11)
    dataframes_list = list(pool.map(ftest.factor_statistics_N, combined_factor_list))  # 需要返回为dataframes格式

    first_dfs = [tuple_df[0] for tuple_df in dataframes_list]
    factor_test_statistics = pd.concat(first_dfs, axis=0)
    second_dfs = [tuple_df[1] for tuple_df in dataframes_list]
    factor_cum_ic = pd.concat(second_dfs, axis=1)
    third_dfs = [tuple_df[2] for tuple_df in dataframes_list]
    factor_LE_cum_ret = pd.concat(third_dfs, axis=1)
    fourth_dfs = [tuple_df[3] for tuple_df in dataframes_list]
    factor_LS_cum_ret = pd.concat(fourth_dfs, axis=1)

    with pd.ExcelWriter(f'{file_name_out}_因子测试报表_.xlsx') as writer:
        factor_test_statistics.to_excel(writer, sheet_name='factor_test_statistics')
        factor_cum_ic.to_excel(writer, sheet_name='factor_cum_ic')
        factor_LE_cum_ret.to_excel(writer, sheet_name='factor_LE_cum_ret')
        factor_LS_cum_ret.to_excel(writer, sheet_name='factor_LS_cum_ret')
